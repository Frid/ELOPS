/*
 * aes.h
 *
 *  Created on: 26. 8. 2015.
 *      Author: nfrid
 */

#ifndef AES_H_
#define AES_H_
/*
	AES implementation - CBC variant

	currently supports only 128 bit key
*/


// The number of columns comprising a state in AES. This is a constant in AES. Value=4
#define Nb 4

// xtime is a macro that finds the product of {02} and the argument to xtime modulo {1b}
#define xtime(x)   ((x<<1) ^ (((x>>7) & 1) * 0x1b))

// The number of rounds in AES Cipher.
#define Nr 10		//for 128-bit key

// The number of 32 bit words in the key.
#define Nk 4		//for 128-bit key

extern int sbox[256];
extern int Rcon[255];

int getSBoxValue(int num);
void KeyExpansion(int *iterations, unsigned char *Key, unsigned char *RoundKey, unsigned char *KeyExpansion_temp);
void AddRoundKey(int round, int *iterations, unsigned char *RoundKey, unsigned char *state);
void SubBytes(int *iterations, unsigned char *state);
void ShiftRows(unsigned char *state);
void MixColumns(int *iterations, unsigned char *state);
void Cipher(int *iterations, unsigned char *state, unsigned char *in, unsigned int *RoundKey, unsigned char *out);

void aes_measure();




#endif /* AES_H_ */
