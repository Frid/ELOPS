/*
 * aes.h
 *
 *  Created on: 26. 8. 2015.
 *      Author: nfrid
 */

#ifndef AES_H_
#define AES_H_
/*
	AES implementation - CBC variant

	currently supports only 128 bit key
*/


// The number of columns comprising a state in AES. This is a constant in AES. Value=4
#define Nb 4

// xtime is a macro that finds the product of {02} and the argument to xtime modulo {1b}
#define xtime(x)   ((x<<1) ^ (((x>>7) & 1) * 0x1b))

// The number of rounds in AES Cipher.
#define Nr 10		//for 128-bit key

// The number of 32 bit words in the key.
#define Nk 4		//for 128-bit key



// The array that stores the round keys.
unsigned char RoundKey[240];

// The Key input to the AES Program
unsigned char Key[32];

int iterations;

unsigned char KeyExpansion_temp[4];

int getSBoxValue(int num);
void KeyExpansion();
void AddRoundKey(int round);
void SubBytes();
void ShiftRows();
void MixColumns();
void Cipher();


void aes_measure();

// plaintext - array with plain text
// in - it is the array that holds the block of plain text to be encrypted.
// out - it is the array that holds the output CipherText after encryption.
// state - the array that holds the intermediate results during encryption.
// nr_of_blocks - number of 4x4 blocks in plaintext
unsigned char plaintext[256];//max 16 blocks
unsigned char in[16];
unsigned char out[16];
unsigned char state[4 * 4];
unsigned int nr_of_blocks;


#endif /* AES_H_ */
