/*
 * aes-main.c
 *
 *  Created on: 26. 8. 2015.
 *      Author: nfrid
 */

#include "aes.h"



int main()
{
	int i,j, n;
	unsigned char iv[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	
    //Key
	for (i = 0; i<Nk * 4; i++)
	{
		Key[i] = i;
	}

	//Number of blocks
	nr_of_blocks = 2;

	//Plaintext (32 bytes total)
	j=0x00;
	for (i = 0; i<16; i++)
	{
		plaintext[i] = j;
		j+=0x11;
	}

	j=0;
	for (i = 16; i<32; i++)
	{
		plaintext[i] = j;
		j+=0x11;
	}


	
    // The KeyExpansion routine must be called before encryption.
	KeyExpansion();


	//traverse all blocks
	for (n = 0; n < nr_of_blocks; n++){

		for (j = 0; j < 16; j++)
			in[j] = plaintext[n*16 + j] ^ iv[j];

		// The next function call encrypts the PlainText with the Key using AES algorithm.
		Cipher();


		//output result
		printf("\nText after encryption:\n");
		for (i = 0; i<Nk * 4; i++)
		{
			printf("%02x ", out[i]);
			iv[i] = out[i];
		}
		printf("\n\n");
	}



    return 0;
}
