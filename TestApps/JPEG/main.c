#include "jpeg.h"
#include <stdlib.h>


int main(){
	

	unsigned char *image_c= (unsigned char*)malloc(7680*sizeof(unsigned char));
	int image_len,n;

	unsigned char r,g,b,y,u,v;
	
	FILE *image, *output;
	
		

	image=fopen("lenna.ppm", "rb"); //!!! FIXME  - hard coded
	output=fopen("lennaJPEG.jpeg", "wb");
	
	

	if (image!=NULL){

		fseek(image,15,SEEK_SET);		


		for(n=0; n<7680; n+=3){			//img size: 64*40*3 !!! FIXME  - hard coded
			r= fgetc(image);
			g= fgetc(image);
			b= fgetc(image);

			y=0.299*r + 0.587*g + 0.144*b;			
			u=-0.1687*r - 0.3313*g + 0.5*b +128;
			v=0.5*r - 0.4187*g - 0.0813*b +128;


			image_c[n]=y;
			image_c[n+1]=u;
			image_c[n+2]=v;

		}

		
		fclose(image);
		
		image_len=jpeg_main(image); 
		
		
		fwrite(image_c, 1, image_len, output);
		
	}

	return 0;

}
