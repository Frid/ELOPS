#include <stdio.h>
#include <stdlib.h>


/*****************structs************************/

typedef struct {
	unsigned char field [8][8];
}blocks;

typedef struct {
	 char field [8][8];
}sig_blocks;



/*****************JPEG functions*******************/
int jpeg_main(unsigned char * image);
void Shift(blocks *Y,  sig_blocks *Y1, int nr_block);
void DCT(sig_blocks *Y, int nr_block);
void DCT_block(short * data);
void ZigZag(sig_blocks *Y, char * zz_seqY, int nr_block);
int CreateImage(unsigned char *out, unsigned char * huff_code, int len_huff);

/*****************Huffman functions*****************/
int HuffEncode(char* zz_seqY, char * zz_seqU, char *zz_seqV, unsigned char *huff_code,int block_count );
void huff_dc_lum(char *zz_seq, char prev_val, unsigned char *huff_code, int *nr_byte, int *nr_bit);
void huff_ac_lum(char *zz_seq, unsigned char *huff_code, int *nr_byte, int *nr_bit);
void huff_dc_chrom(char *zz_seq,  char prev_val,unsigned char *huff_code, int *nr_byte, int *nr_bit);
void huff_ac_chrom(char *zz_seq, unsigned char *huff_code, int *nr_byte, int *nr_bit);



/*****************AUX func****************/

void CreateBlocks(unsigned char *image, blocks *Y, blocks *U, blocks *V);
