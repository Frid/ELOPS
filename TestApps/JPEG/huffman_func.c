/*
 * huffman.c
 *
 *  Created on: 7.12.2012.
 *      Author: Nikolina
 */
#include "jpeg.h"

static const unsigned int dc_lum[12][2]={2,0,3,2,3,3,3,4,3,5,3,6,4,14,5,30,6,62,7,126,8,254,9,510};
static const unsigned int ac_lum[16][11][2]={4, 10, 2, 0, 2, 1, 3, 4, 4, 11, 5, 26, 7, 120, 8, 248, 10, 1014, 16, 65410, 16, 65411, 0, 0, 4, 12, 5, 27, 7, 121, 9, 502, 11, 2038, 16, 65412, 16, 65413, 16, 65414, 16, 65415, 16, 65416, 0, 0, 5, 28, 8, 249, 10, 1015, 12, 4084, 16, 65417, 16, 65418, 16, 65419, 16, 65420, 16, 65421, 16, 65422, 0, 0, 6, 58, 9, 503, 12, 4085, 16, 65423, 16, 65424, 16, 65425, 16, 65426, 16, 65427, 16, 65428, 16, 65429, 0, 0, 6, 59, 10, 1016, 16, 65430, 16, 65431, 16, 65432, 16, 65433, 16, 65434, 16, 65435, 16, 65436, 16, 65437, 0, 0, 7, 122, 11, 2039, 16, 65438, 16, 65439, 16, 65440, 16, 65441, 16, 65442, 16, 65443, 16, 65444, 16, 65445, 0, 0, 7, 123, 12, 4086, 16, 65446, 16, 65447, 16, 65448, 16, 65449, 16, 65450, 16, 65451, 16, 65452, 16, 65453, 0, 0, 8, 250, 12, 4087, 16, 65454, 16, 65455, 16, 65456, 16, 65457, 16, 65458, 16, 65459, 16, 65460, 16, 65461, 0, 0, 9, 504, 15, 32704, 16, 65462, 16, 65463, 16, 65464, 16, 65465, 16, 65466, 16, 65467, 16, 65468, 16, 65469, 0, 0, 9, 505, 16, 65470, 16, 65471, 16, 65472, 16, 65473, 16, 65474, 16, 65475, 16, 65476, 16, 65477, 16, 65478, 0, 0, 9, 506, 16, 65479, 16, 65480, 16, 65481, 16, 65482, 16, 65483, 16, 65484, 16, 65485, 16, 65486, 16, 65487, 0, 0, 10, 1017, 16, 65488, 16, 65489, 16, 65490, 16, 65491, 16, 65492, 16, 65493, 16, 65494, 16, 65495, 16, 65496, 0, 0, 10, 1018, 16, 65497, 16, 65498, 16, 65499, 16, 65500, 16, 65501, 16, 65502, 16, 65503, 16, 65504, 16, 65505, 0, 0, 11, 2040, 16, 65506, 16, 65507, 16, 65508, 16, 65509, 16, 65510, 16, 65511, 16, 65512, 16, 65513, 16, 65514, 0, 0, 16, 65515, 16, 65516, 16, 65517, 16, 65518, 16, 65519, 16, 65520, 16, 65521, 16, 65522, 16, 65523, 16, 65524, 11, 2041, 16, 65525, 16, 65526, 16, 65527, 16, 65528, 16, 65529, 16, 65530, 16, 65531, 16, 65532, 16, 65533, 16, 65534};
static const unsigned int dc_chrom[12][2]={2,0,2,1,2,2,3,6,4,14,5,30,6,62,7,126,8,254,9,510,10,1022,11,2046};
static const unsigned int ac_chrom[16][11][2]={2, 0, 2, 1, 3, 4, 4, 10, 5, 24, 5, 25, 6, 56, 7, 120, 9, 500, 10, 1014, 12, 4084, 0, 0, 4, 11, 6, 57, 8, 246, 9, 501, 11, 2038, 12, 4085, 16, 65416, 16, 65417, 16, 65418, 16, 65419, 0, 0, 5, 26, 8, 247, 10, 1015, 12, 4086, 15, 32706, 16, 65420, 16, 65421, 16, 65422, 16, 65423, 16, 65424, 0, 0, 5, 27, 8, 248, 10, 1016, 12, 4087, 16, 65425, 16, 65426, 16, 65427, 16, 65428, 16, 65429, 16, 65430, 0, 0, 6, 58, 9, 502, 16, 65431, 16, 65432, 16, 65433, 16, 65434, 16, 65435, 16, 65436, 16, 65437, 16, 65438, 0, 0, 6, 59, 10, 1017, 16, 65439, 16, 65440, 16, 65441, 16, 65442, 16, 65443, 16, 65444, 16, 65445, 16, 65446, 0, 0, 7, 121, 11, 2039, 16, 65447, 16, 65448, 16, 65449, 16, 65450, 16, 65451, 16, 65452, 16, 65453, 16, 65454, 0, 0, 7, 122, 11, 2040, 16, 65455, 16, 65456, 16, 65457, 16, 65458, 16, 65459, 16, 65460, 16, 65461, 16, 65462, 0, 0, 8, 249, 16, 65463, 16, 65464, 16, 65465, 16, 65466, 16, 65467, 16, 65468, 16, 65469, 16, 65470, 16, 65471, 0, 0, 9, 503, 16, 65472, 16, 65473, 16, 65474, 16, 65475, 16, 65476, 16, 65477, 16, 65478, 16, 65479, 16, 65480, 0, 0, 9, 504, 16, 65481, 16, 65482, 16, 65483, 16, 65484, 16, 65485, 16, 65486, 16, 65487, 16, 65488, 16, 65489, 0, 0, 9, 505, 16, 65490, 16, 65491, 16, 65492, 16, 65493, 16, 65494, 16, 65495, 16, 65496, 16, 65497, 16, 65498, 0, 0, 9, 506, 16, 65499, 16, 65500, 16, 65501, 16, 65502, 16, 65503, 16, 65504, 16, 65505, 16, 65506, 16, 65507, 0, 0, 11, 2041, 16, 65508, 16, 65509, 16, 65510, 16, 65511, 16, 65512, 16, 65513, 16, 65514, 16, 65515, 16, 65516, 0, 0, 14, 16352, 16, 65517, 16, 65518, 16, 65519, 16, 65520, 16, 65521, 16, 65522, 16, 65523, 16, 65524, 16, 65525, 10, 1018, 15, 32707, 16, 65526, 16, 65527, 16, 65528, 16, 65529, 16, 65530, 16, 65531, 16, 65532, 16, 65533, 16, 65534};


int HuffEncode(char* zz_seqY, char * zz_seqU, char *zz_seqV, unsigned char *huff_code,int block_count ){
	int n;
	int nr_byte=0, nr_bit=0;
	unsigned char *kod=(unsigned char*)malloc(20000*sizeof(unsigned char));
	char prev_val_Y=0, prev_val_U=0, prev_val_V=0 ;

	//if(kod ==NULL) print("kodNULL");
	//else print("kod OK");
	kod[0]=0x00;

	for(n=0; n<block_count; n++){
		//Y komponenta


			//DC huff
			huff_dc_lum((zz_seqY+n*64), prev_val_Y, kod,  &nr_byte,  &nr_bit);

			//AC huff
			huff_ac_lum((zz_seqY+n*64), kod, &nr_byte, &nr_bit);

		//U komp

			//DC huff
			huff_dc_chrom((zz_seqU+n*64), prev_val_U, kod, &nr_byte, &nr_bit);

			//AC huff
			huff_ac_chrom((zz_seqU+n*64),  kod, &nr_byte, &nr_bit);

		//V komp

			//DC huff
			huff_dc_chrom((zz_seqV+n*64), prev_val_V, kod, &nr_byte, &nr_bit);

			//AC huff
			huff_ac_chrom((zz_seqV+n*64), kod, &nr_byte, &nr_bit);


			prev_val_Y=zz_seqY[n*64];
			prev_val_U=zz_seqU[n*64];
			prev_val_V=zz_seqV[n*64];


	}

		nr_bit=0;
	for (n=0; n<=nr_byte; n++){
		huff_code[nr_bit]=kod[n];
		nr_bit++;
		if(kod[n]==0xff){ huff_code[nr_bit]=0x00;nr_bit++; }
	}

		free(kod);
		return nr_byte+1;
	}


void huff_dc_lum(char *zz_blok, char prev_val, unsigned char *huff_code, int *nr_byte, int *nr_bit){
int k=0;
char razlika;
unsigned char kategorija;
unsigned int kodna_rijec;

int i,c;



	i=*(nr_byte);
	c=*(nr_bit);
    razlika=zz_blok[0] - prev_val;


	//DC luminance

	if (razlika == 0) kategorija=0;
	else if (razlika >= -1 && razlika<=1) kategorija=1;
	else if (razlika >= -3  &&  razlika<= 3) kategorija=2;
	else if (razlika >= -7  && razlika<= 7) kategorija=3;
	else if (razlika >= -15 && razlika<= 15) kategorija=4;
	else if (razlika >= -31  && razlika<= 31) kategorija=5;
	else if (razlika >= -63 &&  razlika<= 63) kategorija=6;
	else if (razlika >= -127  &&  razlika<= 127) kategorija=7;
	else if (razlika >= -255 && razlika<= 255) kategorija=8;
	else if (razlika >= -511 && razlika<= 511) kategorija=9;
	else if (razlika >= -1023 && razlika<= 1023) kategorija=10;
	else  kategorija=11;


	kodna_rijec=dc_lum[kategorija][1];

	while (k<dc_lum[kategorija][0]){

		huff_code[i]|=(kodna_rijec>>(dc_lum[kategorija][0]-k-1)) & 0x01;

		if (c==7){
			 huff_code[i+1]=0x00; i++; c=0;}
		else { huff_code[i]= huff_code[i]<<1; c++;}
		k++;
	}

	k=0;
	if (razlika<0) razlika -=1;


	while (k<kategorija){
		huff_code[i]|=(razlika>>(kategorija-k-1)) & 1;

		if (c==7){  huff_code[i+1]=0x00;i++; c=0; huff_code[i]=0x00;}
		else { huff_code[i]= huff_code[i]<<1; c++;}
		k++;
	}

	*nr_byte=i;
	*nr_bit=c;


}


void huff_ac_lum(char *zz_blok, unsigned char *huff_code, int *nr_byte, int *nr_bit){

 int j=0, br_nula=0, br_zrl=0, k=0;
 char koeficijent;
 unsigned char kategorija;
 unsigned int kodna_rijec;

 int i,c;

	i=*(nr_byte);
	c=*(nr_bit);

	j=1;
	while(j<64){
		//ako je koeficijent 0
		if (zz_blok[j]==0){
			br_nula++;

			if (br_nula==16){ br_nula=0; br_zrl++;} //ako ima 16 nula u nizu

			if(j==63){  kodna_rijec=ac_lum[0][0][1];//EOB

						  k=0;
							  while (k<ac_lum[0][0][0]){
										huff_code[i]|=(kodna_rijec>>(ac_lum[0][0][0]-k-1)) & 1;
										if (c==7){ huff_code[i+1]=0x00;i++; c=0;}
										else { huff_code[i]= huff_code[i]<<1; c++;}
										k++;
							}
			}
		}

		//ako je koeficijent razlicit od 0
		else{
			koeficijent=zz_blok[j];

			if (koeficijent >= -1 && koeficijent<=1) kategorija=1;
			else if (koeficijent >= -3  &&  koeficijent<= 3) kategorija=2;
			else if (koeficijent >= -7  && koeficijent<= 7) kategorija=3;
			else if (koeficijent >= -15 && koeficijent<= 15) kategorija=4;
			else if (koeficijent >= -31  && koeficijent<= 31) kategorija=5;
			else if (koeficijent >= -63 &&  koeficijent<= 63) kategorija=6;
			else if (koeficijent >= -127  &&  koeficijent<= 127) kategorija=7;
			else if (koeficijent >= -255 && koeficijent<= 255) kategorija=8;
			else if (koeficijent >= -511 && koeficijent<= 511) kategorija=9;
			else  kategorija=10;

			//upisi sve zaostale ZRL ako ih ima
			while(br_zrl>0){
				k=0;
				kodna_rijec=ac_lum[15][0][0];

						  while (k<ac_lum[15][0][0]){
										huff_code[i]|=(kodna_rijec>>(ac_lum[15][0][0]-k-1)) & 1;
										if (c==7){  huff_code[i+1]=0x00;i++; c=0;}
										else { huff_code[i]= huff_code[i]<<1; c++;}
										k++;
							}
               br_zrl--;
			}

			//upisi kodnu rijec koeficijenta
			kodna_rijec=ac_lum[br_nula][kategorija][1];

			k=0;
			while (k<ac_lum[br_nula][kategorija][0]){
				huff_code[i]|=(kodna_rijec>>(ac_lum[br_nula][kategorija][0]-k-1)) & 1;
				if (c==7){huff_code[i+1]=0x00;i++; c=0; }
				else { huff_code[i]= huff_code[i]<<1; c++;}
				k++;
			}


		    if (zz_blok[ j]<0) zz_blok[j] -= 1;

			k=0;
			while (k<kategorija){
				huff_code[i]|=(zz_blok[j]>>(kategorija-k-1)) & 1;
				if (c==7){ huff_code[i+1]=0x00;i++; c=0;}
				else { huff_code[i]= huff_code[i]<<1; c++;}
				k++;
			}
			br_nula=0;

		}
		j++;
	}




	*nr_byte=i;
	*nr_bit=c;
}



void huff_dc_chrom(char *zz_blok,  char prev_val,unsigned char *huff_code, int *nr_byte, int *nr_bit){

int k=0;
char razlika;
unsigned char kategorija;
unsigned int kodna_rijec;

int i,c;

	i=*(nr_byte);
	c=*(nr_bit);
    razlika=zz_blok[0] - prev_val;

	//DC chrominance

	if (razlika == 0) kategorija=0;
	else if (razlika >= -1 && razlika<=1) kategorija=1;
	else if (razlika >= -3  &&  razlika<= 3) kategorija=2;
	else if (razlika >= -7  && razlika<= 7) kategorija=3;
	else if (razlika >= -15 && razlika<= 15) kategorija=4;
	else if (razlika >= -31  && razlika<= 31) kategorija=5;
	else if (razlika >= -63 &&  razlika<= 63) kategorija=6;
	else if (razlika >= -127  &&  razlika<= 127) kategorija=7;
	else if (razlika >= -255 && razlika<= 255) kategorija=8;
	else if (razlika >= -511 && razlika<= 511) kategorija=9;
	else if (razlika >= -1023 && razlika<= 1023) kategorija=10;
	else  kategorija=11;

	kodna_rijec=dc_chrom[kategorija][1];

	while (k<dc_chrom[kategorija][0]){
		huff_code[i]|=(kodna_rijec>>(dc_chrom[kategorija][0]-k-1)) & 1;
		if (c==7){ huff_code[i+1]=0x00; i++; c=0;}
		else { huff_code[i]= huff_code[i]<<1; c++;}
		k++;
	}

	k=0;
	if (razlika<0) razlika -= 1;

	while (k<kategorija){
		huff_code[i]|=(razlika>>(kategorija-k-1)) & 1;
		if (c==7){huff_code[i+1]=0x00; i++; c=0;}
		else { huff_code[i]= huff_code[i]<<1; c++;}
		k++;
	}


	*nr_byte=i;
	*nr_bit=c;
}


void huff_ac_chrom(char *zz_blok, unsigned char *huff_code, int *nr_byte, int *nr_bit){

int j=0, br_nula=0, br_zrl=0, k=0;
 char koeficijent;
 unsigned char kategorija;
 unsigned int kodna_rijec;

 int i,c;

	i=*(nr_byte);
	c=*(nr_bit);

	j=1;
	while(j<64){
		//ako je koeficijent 0
		if (zz_blok[j]==0){
			br_nula++;

			if (br_nula==16){ br_nula=0; br_zrl++;} //ako ima 16 nula u nizu

			if(j==63){  kodna_rijec=ac_chrom[0][0][1];//EOB

							  k=0;
							  while (k<ac_chrom[0][0][0]){
										huff_code[i]|=(kodna_rijec>>(ac_chrom[0][0][0]-k-1)) & 1;
										if (c==7){  huff_code[i+1]=0x00; i++; c=0;}
										else { huff_code[i]= huff_code[i]<<1; c++;}
										k++;
							}
			}
		}

		//ako je koeficijent razlicit od 0
		else{
			koeficijent=zz_blok[j];

			if (koeficijent >= -1 && koeficijent<=1) kategorija=1;
			else if (koeficijent >= -3  &&  koeficijent<= 3) kategorija=2;
			else if (koeficijent >= -7  && koeficijent<= 7) kategorija=3;
			else if (koeficijent >= -15 && koeficijent<= 15) kategorija=4;
			else if (koeficijent >= -31  && koeficijent<= 31) kategorija=5;
			else if (koeficijent >= -63 &&  koeficijent<= 63) kategorija=6;
			else if (koeficijent >= -127  &&  koeficijent<= 127) kategorija=7;
			else if (koeficijent >= -255 && koeficijent<= 255) kategorija=8;
			else if (koeficijent >= -511 && koeficijent<= 511) kategorija=9;
			else  kategorija=10;

			//upisi sve zaostale ZRL ako ih ima
			while(br_zrl>0){
				k=0;
				kodna_rijec=ac_chrom[15][0][0];

						  while (k<ac_chrom[15][0][0]){
										huff_code[i]|=(kodna_rijec>>(ac_chrom[15][0][0]-k-1)) & 1;
										if (c==7){ huff_code[i+1]=0x00;i++; c=0;}
										else { huff_code[i]= huff_code[i]<<1; c++;}
										k++;
							}
               br_zrl--;
			}

			//upisi kodnu rijec koeficijenta
			kodna_rijec=ac_chrom[br_nula][kategorija][1];

			k=0;
			while (k<ac_chrom[br_nula][kategorija][0]){
				huff_code[i]|=(kodna_rijec>>(ac_chrom[br_nula][kategorija][0]-k-1)) & 1;
				if (c==7){ huff_code[i+1]=0x00;i++; c=0;}
				else { huff_code[i]= huff_code[i]<<1; c++;}
				k++;
			}


		    if (zz_blok[ j]<0) zz_blok[j] -= 1;

			k=0;
			while (k<kategorija){
				huff_code[i]|=(zz_blok[j]>>(kategorija-k-1)) & 1;
				if (c==7){  huff_code[i+1]=0x00;i++; c=0;}
				else { huff_code[i]= huff_code[i]<<1; c++;}
				k++;
			}
			br_nula=0;

		}
		j++;
	}




	*nr_byte=i;
	*nr_bit=c;

}
