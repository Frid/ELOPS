#include "jpeg.h"
#include "string.h"

int jpeg_main(unsigned char * image){

	
		
	blocks *Y, *U, *V=NULL;
	sig_blocks *Y1, *U1, *V1=NULL;

	char *zz_seqY=NULL;
	char *zz_seqU=NULL;
	char *zz_seqV=NULL;

	unsigned char* huff_kod=NULL;
	int len_huff=0;

	int nr_block=40; //!!!FIXME hardcoded!!!

	int img_len=0;

	

	Y=(blocks*)malloc(nr_block*sizeof(blocks));
	if (Y==NULL){(printf("NULL!"); return;}
	U=(blocks*)malloc(nr_block*sizeof(blocks));
	if (U==NULL){(printf("NULL!"); return;}
	V=(blocks*)malloc(nr_block*sizeof(blocks));
	if (V==NULL){(printf("NULL!"); return;}
	Y1=(sig_blocks*)malloc(nr_block*sizeof(sig_blocks));
	if (Y1==NULL){(printf("NULL!"); return;}
	U1=(sig_blocks*)malloc(nr_block*sizeof(sig_blocks));
	if (U1==NULL){(printf("NULL!"); return;}
	V1=(sig_blocks*)malloc(nr_block*sizeof(sig_blocks));
	if (V1==NULL){(printf("NULL!"); return;}
	zz_seqY=(char*)malloc(nr_block*64*sizeof(char));
	if (zz_seqY==NULL){(printf("NULL!"); return;}
	zz_seqU=(char*)malloc(nr_block*64*sizeof(char));
	if (zz_seqU==NULL){(printf("NULL!"); return;}
	zz_seqV=(char*)malloc(nr_block*64*sizeof(char));
	if (zz_seqV==NULL){(printf("NULL!"); return;}
	huff_kod=(unsigned char*)malloc(10000*sizeof(unsigned char)); //!!!FIXME hardoced!!!


	if (huff_kod==NULL){(printf("NULL!"); return;}

	
	CreateBlocks(image, Y, U, V);
	Shift(Y,Y1,nr_block);
	Shift(U,U1,nr_block);
	Shift(V,V1,nr_block);
	
	DCT(Y1, nr_block);	
	DCT(U1, nr_block);
	DCT(V1, nr_block);

	ZigZag(Y1, zz_seqY, nr_block);
	ZigZag(U1, zz_seqU, nr_block);
	ZigZag(V1, zz_seqV, nr_block);

	len_huff=HuffEncode(zz_seqY,zz_seqU,zz_seqV, huff_kod, nr_block);

	img_len=CreateImage(image,huff_kod, len_huff);



	free(huff_kod);

	return img_len;

}
