#include "FP_ELOPS.h"

void fp_elops_par_var_ADD(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y) {

	int i = 0;


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "FP_par_var - ADD");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - ADD|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*b) + (*c);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "FP_par_var - ADD|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*c) + (*b) + (*d);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "FP_par_var - ADD|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*b) + (*c) + (*e) + (*d) + (*f);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "FP_par_var - ADD|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*b) + (*c) + (*e) + (*d) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - ADD|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*c);
		(*b) = (*a) + (*b);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "FP_par_var - ADD|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*c);
		(*b) = (*a) + (*b);
		(*d) = (*a) + (*d);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "FP_par_var - ADD|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*c);
		(*b) = (*a) + (*b);
		(*d) = (*a) + (*d);
		(*e) = (*a) + (*e);
		(*f) = (*a) + (*f);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "FP_par_var - ADD|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) + (*c);
		(*b) = (*a) + (*b);
		(*d) = (*a) + (*d);
		(*e) = (*a) + (*e);
		(*f) = (*a) + (*f);
		(*g) = (*a) + (*g);
		(*h) = (*a) + (*h);
		(*j) = (*a) + (*j);
		(*x) = (*a) + (*x);
		(*y) = (*a) + (*y);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);




	strcpy(op_name, "FP_par_var - ADD|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - ADD|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + 25;
		(*b) = (*b) + 75;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "FP_par_var - ADD|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + 25;
		(*b) = (*b) + 75;
		(*d) = (*d) + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "FP_par_var - ADD|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + 25;
		(*b) = (*b) + 75;
		(*d) = (*d) + 25;
		(*e) = (*e) + 75;
		(*f) = (*f) + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "FP_par_var - ADD|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + 25;
		(*b) = (*b) + 75;
		(*d) = (*d) + 25;
		(*e) = (*e) + 75;
		(*f) = (*f) + 25;
		(*g) = (*g) + 25;
		(*h) = (*h) + 75;
		(*j) = (*j) + 25;
		(*x) = (*x) + 75;
		(*y) = (*y) + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);



	strcpy(op_name, "FP_par_var - ADD|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - ADD|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + i;
		(*b) = (*b) + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "FP_par_var - ADD|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + i;
		(*b) = (*b) + i;
		(*d) = (*d) + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "FP_par_var - ADD|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + i;
		(*b) = (*b) + i;
		(*d) = (*d) + i;
		(*e) = (*e) + i;
		(*f) = (*f) + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "FP_par_var - ADD|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + i;
		(*b) = (*b) + i;
		(*d) = (*d) + i;
		(*e) = (*e) + i;
		(*f) = (*f) + i;
		(*g) = (*g) + i;
		(*h) = (*h) + i;
		(*j) = (*j) + i;
		(*x) = (*x) + i;
		(*y) = (*y) + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);



	strcpy(op_name, "FP_par_var - ADD|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - ADD|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + g_a;
		(*b) = (*b) + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "FP_par_var - ADD|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + g_a;
		(*b) = (*b) + g_a;
		(*d) = (*d) + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "FP_par_var - ADD|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + g_a;
		(*b) = (*b) + g_a;
		(*d) = (*d) + g_a;
		(*e) = (*e) + g_a;
		(*f) = (*f) + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "FP_par_var - ADD|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) + g_a;
		(*b) = (*b) + g_a;
		(*d) = (*d) + g_a;
		(*e) = (*e) + g_a;
		(*f) = (*f) + g_a;
		(*g) = (*g) + g_a;
		(*h) = (*h) + g_a;
		(*j) = (*j) + g_a;
		(*x) = (*x) + g_a;
		(*y) = (*y) + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);

}

void fp_elops_par_arr_ADD_l1(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y, double *z) {

	int i = 0;
	double lf[1000];


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + g[i] + h[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + g[i] + h[i] + j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + g[i] + h[i] + j[i] + x[i] + y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] + h[i];
		g[i] = f[i] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] + h[i];
		g[i] = f[i] + g[i];
		j[i] = f[i] + j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] + h[i];
		g[i] = f[i] + g[i];
		j[i] = f[i] + j[i];
		x[i] = f[i] + x[i];
		y[i] = f[i] + y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	};



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] + h[i];
		g[i] = f[i] + g[i];
		j[i] = f[i] + j[i];
		x[i] = f[i] + x[i];
		y[i] = f[i] + y[i];
		b[i] = f[i] + b[i];
		c[i] = f[i] + c[i];
		d[i] = f[i] + d[i];
		e[i] = f[i] + e[i];
		z[i] = f[i] + z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + 25;
		g[i] = g[i] + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + 25;
		g[i] = g[i] + 25;
		j[i] = j[i] + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + 25;
		g[i] = g[i] + 25;
		j[i] = j[i] + 25;
		x[i] = x[i] + 25;
		y[i] = y[i] + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + 25;
		g[i] = g[i] + 25;
		j[i] = j[i] + 25;
		x[i] = x[i] + 25;
		y[i] = y[i] + 25;
		b[i] = b[i] + 25;
		c[i] = c[i] + 25;
		d[i] = d[i] + 25;
		e[i] = e[i] + 25;
		z[i] = z[i] + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + (*a);
		g[i] = g[i] + (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + (*a);
		g[i] = g[i] + (*a);
		j[i] = j[i] + (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + (*a);
		g[i] = g[i] + (*a);
		j[i] = j[i] + (*a);
		x[i] = x[i] + (*a);
		y[i] = y[i] + (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + (*a);
		g[i] = g[i] + (*a);
		j[i] = j[i] + (*a);
		x[i] = x[i] + (*a);
		y[i] = y[i] + (*a);
		b[i] = b[i] + (*a);
		c[i] = c[i] + (*a);
		d[i] = d[i] + (*a);
		e[i] = e[i] + (*a);
		z[i] = z[i] + (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + i;
		g[i] = g[i] + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + i;
		g[i] = g[i] + i;
		j[i] = j[i] + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + i;
		g[i] = g[i] + i;
		j[i] = j[i] + i;
		x[i] = x[i] + i;
		y[i] = y[i] + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + i;
		g[i] = g[i] + i;
		j[i] = j[i] + i;
		x[i] = x[i] + i;
		y[i] = y[i] + i;
		b[i] = b[i] + i;
		c[i] = c[i] + i;
		d[i] = d[i] + i;
		e[i] = e[i] + i;
		z[i] = z[i] + i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + lf[i];
		g[i] = g[i] + lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + lf[i];
		g[i] = g[i] + lf[i];
		j[i] = j[i] + lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + lf[i];
		g[i] = g[i] + lf[i];
		j[i] = j[i] + lf[i];
		x[i] = x[i] + lf[i];
		y[i] = y[i] + lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + lf[i];
		g[i] = g[i] + lf[i];
		j[i] = j[i] + lf[i];
		x[i] = x[i] + lf[i];
		y[i] = y[i] + lf[i];
		b[i] = b[i] + lf[i];
		c[i] = c[i] + lf[i];
		d[i] = d[i] + lf[i];
		e[i] = e[i] + lf[i];
		z[i] = z[i] + lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + g_a;
		g[i] = g[i] + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + g_a;
		g[i] = g[i] + g_a;
		j[i] = j[i] + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + g_a;
		g[i] = g[i] + g_a;
		j[i] = j[i] + g_a;
		x[i] = x[i] + g_a;
		y[i] = y[i] + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + g_a;
		g[i] = g[i] + g_a;
		j[i] = j[i] + g_a;
		x[i] = x[i] + g_a;
		y[i] = y[i] + g_a;
		b[i] = b[i] + g_a;
		c[i] = c[i] + g_a;
		d[i] = d[i] + g_a;
		e[i] = e[i] + g_a;
		z[i] = z[i] + g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] + gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + gf_f[i];
		g[i] = g[i] + gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + gf_f[i];
		g[i] = g[i] + gf_f[i];
		j[i] = j[i] + gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + gf_f[i];
		g[i] = g[i] + gf_f[i];
		j[i] = j[i] + gf_f[i];
		x[i] = x[i] + gf_f[i];
		y[i] = y[i] + gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=1|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] + gf_f[i];
		g[i] = g[i] + gf_f[i];
		j[i] = j[i] + gf_f[i];
		x[i] = x[i] + gf_f[i];
		y[i] = y[i] + gf_f[i];
		b[i] = b[i] + gf_f[i];
		c[i] = c[i] + gf_f[i];
		d[i] = d[i] + gf_f[i];
		e[i] = e[i] + gf_f[i];
		z[i] = z[i] + gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}

}

void fp_elops_par_arr_ADD_l2(double *a, double b[30][30], double c[30][30], double d[30][30], double e[30][30], double f[30][30], double g[30][30], double h[30][30], double j[30][30], double x[30][30], double y[30][30], double z[30][30]) {

	int i = 0, k = 0;
	double lf[30][30];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + g[i][k] + h[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + g[i][k] + h[i][k] + j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] + h[i][k];
			g[i][k] = f[i][k] + g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] + h[i][k];
			g[i][k] = f[i][k] + g[i][k];
			j[i][k] = f[i][k] + j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] + h[i][k];
			g[i][k] = f[i][k] + g[i][k];
			j[i][k] = f[i][k] + j[i][k];
			x[i][k] = f[i][k] + x[i][k];
			y[i][k] = f[i][k] + y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	};



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] + h[i][k];
			g[i][k] = f[i][k] + g[i][k];
			j[i][k] = f[i][k] + j[i][k];
			x[i][k] = f[i][k] + x[i][k];
			y[i][k] = f[i][k] + y[i][k];
			b[i][k] = f[i][k] + b[i][k];
			c[i][k] = f[i][k] + c[i][k];
			d[i][k] = f[i][k] + d[i][k];
			e[i][k] = f[i][k] + e[i][k];
			z[i][k] = f[i][k] + z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + 25;
			g[i][k] = g[i][k] + 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + 25;
			g[i][k] = g[i][k] + 25;
			j[i][k] = j[i][k] + 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + 25;
			g[i][k] = g[i][k] + 25;
			j[i][k] = j[i][k] + 25;
			x[i][k] = x[i][k] + 25;
			y[i][k] = y[i][k] + 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + 25;
			g[i][k] = g[i][k] + 25;
			j[i][k] = j[i][k] + 25;
			x[i][k] = x[i][k] + 25;
			y[i][k] = y[i][k] + 25;
			b[i][k] = b[i][k] + 25;
			c[i][k] = c[i][k] + 25;
			d[i][k] = d[i][k] + 25;
			e[i][k] = e[i][k] + 25;
			z[i][k] = z[i][k] + 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + (*a);
			g[i][k] = g[i][k] + (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + (*a);
			g[i][k] = g[i][k] + (*a);
			j[i][k] = j[i][k] + (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + (*a);
			g[i][k] = g[i][k] + (*a);
			j[i][k] = j[i][k] + (*a);
			x[i][k] = x[i][k] + (*a);
			y[i][k] = y[i][k] + (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + (*a);
			g[i][k] = g[i][k] + (*a);
			j[i][k] = j[i][k] + (*a);
			x[i][k] = x[i][k] + (*a);
			y[i][k] = y[i][k] + (*a);
			b[i][k] = b[i][k] + (*a);
			c[i][k] = c[i][k] + (*a);
			d[i][k] = d[i][k] + (*a);
			e[i][k] = e[i][k] + (*a);
			z[i][k] = z[i][k] + (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + i;
			g[i][k] = g[i][k] + i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + i;
			g[i][k] = g[i][k] + i;
			j[i][k] = j[i][k] + i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + i;
			g[i][k] = g[i][k] + i;
			j[i][k] = j[i][k] + i;
			x[i][k] = x[i][k] + i;
			y[i][k] = y[i][k] + i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + i;
			g[i][k] = g[i][k] + i;
			j[i][k] = j[i][k] + i;
			x[i][k] = x[i][k] + i;
			y[i][k] = y[i][k] + i;
			b[i][k] = b[i][k] + i;
			c[i][k] = c[i][k] + i;
			d[i][k] = d[i][k] + i;
			e[i][k] = e[i][k] + i;
			z[i][k] = z[i][k] + i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + lf[i][k];
			g[i][k] = g[i][k] + lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + lf[i][k];
			g[i][k] = g[i][k] + lf[i][k];
			j[i][k] = j[i][k] + lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + lf[i][k];
			g[i][k] = g[i][k] + lf[i][k];
			j[i][k] = j[i][k] + lf[i][k];
			x[i][k] = x[i][k] + lf[i][k];
			y[i][k] = y[i][k] + lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + lf[i][k];
			g[i][k] = g[i][k] + lf[i][k];
			j[i][k] = j[i][k] + lf[i][k];
			x[i][k] = x[i][k] + lf[i][k];
			y[i][k] = y[i][k] + lf[i][k];
			b[i][k] = b[i][k] + lf[i][k];
			c[i][k] = c[i][k] + lf[i][k];
			d[i][k] = d[i][k] + lf[i][k];
			e[i][k] = e[i][k] + lf[i][k];
			z[i][k] = z[i][k] + lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + g_a;
			g[i][k] = g[i][k] + g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + g_a;
			g[i][k] = g[i][k] + g_a;
			j[i][k] = j[i][k] + g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + g_a;
			g[i][k] = g[i][k] + g_a;
			j[i][k] = j[i][k] + g_a;
			x[i][k] = x[i][k] + g_a;
			y[i][k] = y[i][k] + g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + g_a;
			g[i][k] = g[i][k] + g_a;
			j[i][k] = j[i][k] + g_a;
			x[i][k] = x[i][k] + g_a;
			y[i][k] = y[i][k] + g_a;
			b[i][k] = b[i][k] + g_a;
			c[i][k] = c[i][k] + g_a;
			d[i][k] = d[i][k] + g_a;
			e[i][k] = e[i][k] + g_a;
			z[i][k] = z[i][k] + g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] + l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + l2_gf_f[i][k];
			g[i][k] = g[i][k] + l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + l2_gf_f[i][k];
			g[i][k] = g[i][k] + l2_gf_f[i][k];
			j[i][k] = j[i][k] + l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + l2_gf_f[i][k];
			g[i][k] = g[i][k] + l2_gf_f[i][k];
			j[i][k] = j[i][k] + l2_gf_f[i][k];
			x[i][k] = x[i][k] + l2_gf_f[i][k];
			y[i][k] = y[i][k] + l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=2|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] + l2_gf_f[i][k];
			g[i][k] = g[i][k] + l2_gf_f[i][k];
			j[i][k] = j[i][k] + l2_gf_f[i][k];
			x[i][k] = x[i][k] + l2_gf_f[i][k];
			y[i][k] = y[i][k] + l2_gf_f[i][k];
			b[i][k] = b[i][k] + l2_gf_f[i][k];
			c[i][k] = c[i][k] + l2_gf_f[i][k];
			d[i][k] = d[i][k] + l2_gf_f[i][k];
			e[i][k] = e[i][k] + l2_gf_f[i][k];
			z[i][k] = z[i][k] + l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}

}


void fp_elops_par_arr_ADD_l3(double *a, double b[10][10][10], double c[10][10][10], double d[10][10][10], double e[10][10][10], double f[10][10][10], double g[10][10][10], double h[10][10][10], double j[10][10][10], double x[10][10][10], double y[10][10][10], double z[10][10][10]) {


	int i = 0, k = 0, n = 0;
	double lf[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + g[i][k][n] + h[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] + h[i][k][n];
				g[i][k][n] = f[i][k][n] + g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] + h[i][k][n];
				g[i][k][n] = f[i][k][n] + g[i][k][n];
				j[i][k][n] = f[i][k][n] + j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}




	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] + h[i][k][n];
				g[i][k][n] = f[i][k][n] + g[i][k][n];
				j[i][k][n] = f[i][k][n] + j[i][k][n];
				x[i][k][n] = f[i][k][n] + x[i][k][n];
				y[i][k][n] = f[i][k][n] + y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	};



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] + h[i][k][n];
				g[i][k][n] = f[i][k][n] + g[i][k][n];
				j[i][k][n] = f[i][k][n] + j[i][k][n];
				x[i][k][n] = f[i][k][n] + x[i][k][n];
				y[i][k][n] = f[i][k][n] + y[i][k][n];
				b[i][k][n] = f[i][k][n] + b[i][k][n];
				c[i][k][n] = f[i][k][n] + c[i][k][n];
				d[i][k][n] = f[i][k][n] + d[i][k][n];
				e[i][k][n] = f[i][k][n] + e[i][k][n];
				z[i][k][n] = f[i][k][n] + z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + 25;
				g[i][k][n] = g[i][k][n] + 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + 25;
				g[i][k][n] = g[i][k][n] + 25;
				j[i][k][n] = j[i][k][n] + 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + 25;
				g[i][k][n] = g[i][k][n] + 25;
				j[i][k][n] = j[i][k][n] + 25;
				x[i][k][n] = x[i][k][n] + 25;
				y[i][k][n] = y[i][k][n] + 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + 25;
				g[i][k][n] = g[i][k][n] + 25;
				j[i][k][n] = j[i][k][n] + 25;
				x[i][k][n] = x[i][k][n] + 25;
				y[i][k][n] = y[i][k][n] + 25;
				b[i][k][n] = b[i][k][n] + 25;
				c[i][k][n] = c[i][k][n] + 25;
				d[i][k][n] = d[i][k][n] + 25;
				e[i][k][n] = e[i][k][n] + 25;
				z[i][k][n] = z[i][k][n] + 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + (*a);
				g[i][k][n] = g[i][k][n] + (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + (*a);
				g[i][k][n] = g[i][k][n] + (*a);
				j[i][k][n] = j[i][k][n] + (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + (*a);
				g[i][k][n] = g[i][k][n] + (*a);
				j[i][k][n] = j[i][k][n] + (*a);
				x[i][k][n] = x[i][k][n] + (*a);
				y[i][k][n] = y[i][k][n] + (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + (*a);
				g[i][k][n] = g[i][k][n] + (*a);
				j[i][k][n] = j[i][k][n] + (*a);
				x[i][k][n] = x[i][k][n] + (*a);
				y[i][k][n] = y[i][k][n] + (*a);
				b[i][k][n] = b[i][k][n] + (*a);
				c[i][k][n] = c[i][k][n] + (*a);
				d[i][k][n] = d[i][k][n] + (*a);
				e[i][k][n] = e[i][k][n] + (*a);
				z[i][k][n] = z[i][k][n] + (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + i;
				g[i][k][n] = g[i][k][n] + i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + i;
				g[i][k][n] = g[i][k][n] + i;
				j[i][k][n] = j[i][k][n] + i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + i;
				g[i][k][n] = g[i][k][n] + i;
				j[i][k][n] = j[i][k][n] + i;
				x[i][k][n] = x[i][k][n] + i;
				y[i][k][n] = y[i][k][n] + i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + i;
				g[i][k][n] = g[i][k][n] + i;
				j[i][k][n] = j[i][k][n] + i;
				x[i][k][n] = x[i][k][n] + i;
				y[i][k][n] = y[i][k][n] + i;
				b[i][k][n] = b[i][k][n] + i;
				c[i][k][n] = c[i][k][n] + i;
				d[i][k][n] = d[i][k][n] + i;
				e[i][k][n] = e[i][k][n] + i;
				z[i][k][n] = z[i][k][n] + i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + lf[i][k][n];
				g[i][k][n] = g[i][k][n] + lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + lf[i][k][n];
				g[i][k][n] = g[i][k][n] + lf[i][k][n];
				j[i][k][n] = j[i][k][n] + lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + lf[i][k][n];
				g[i][k][n] = g[i][k][n] + lf[i][k][n];
				j[i][k][n] = j[i][k][n] + lf[i][k][n];
				x[i][k][n] = x[i][k][n] + lf[i][k][n];
				y[i][k][n] = y[i][k][n] + lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + lf[i][k][n];
				g[i][k][n] = g[i][k][n] + lf[i][k][n];
				j[i][k][n] = j[i][k][n] + lf[i][k][n];
				x[i][k][n] = x[i][k][n] + lf[i][k][n];
				y[i][k][n] = y[i][k][n] + lf[i][k][n];
				b[i][k][n] = b[i][k][n] + lf[i][k][n];
				c[i][k][n] = c[i][k][n] + lf[i][k][n];
				d[i][k][n] = d[i][k][n] + lf[i][k][n];
				e[i][k][n] = e[i][k][n] + lf[i][k][n];
				z[i][k][n] = z[i][k][n] + lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + g_a;
				g[i][k][n] = g[i][k][n] + g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + g_a;
				g[i][k][n] = g[i][k][n] + g_a;
				j[i][k][n] = j[i][k][n] + g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + g_a;
				g[i][k][n] = g[i][k][n] + g_a;
				j[i][k][n] = j[i][k][n] + g_a;
				x[i][k][n] = x[i][k][n] + g_a;
				y[i][k][n] = y[i][k][n] + g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + g_a;
				g[i][k][n] = g[i][k][n] + g_a;
				j[i][k][n] = j[i][k][n] + g_a;
				x[i][k][n] = x[i][k][n] + g_a;
				y[i][k][n] = y[i][k][n] + g_a;
				b[i][k][n] = b[i][k][n] + g_a;
				c[i][k][n] = c[i][k][n] + g_a;
				d[i][k][n] = d[i][k][n] + g_a;
				e[i][k][n] = e[i][k][n] + g_a;
				z[i][k][n] = z[i][k][n] + g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] + l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] + l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] + l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] + l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] + l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] + l3_gf_f[i][k][n];
				x[i][k][n] = x[i][k][n] + l3_gf_f[i][k][n];
				y[i][k][n] = y[i][k][n] + l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - ADD|simple|lvl=3|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] + l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] + l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] + l3_gf_f[i][k][n];
				x[i][k][n] = x[i][k][n] + l3_gf_f[i][k][n];
				y[i][k][n] = y[i][k][n] + l3_gf_f[i][k][n];
				b[i][k][n] = b[i][k][n] + l3_gf_f[i][k][n];
				c[i][k][n] = c[i][k][n] + l3_gf_f[i][k][n];
				d[i][k][n] = d[i][k][n] + l3_gf_f[i][k][n];
				e[i][k][n] = e[i][k][n] + l3_gf_f[i][k][n];
				z[i][k][n] = z[i][k][n] + l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}


}

void fp_elops_par_arr_ADD_cmplx(double *f, double *g) {


	int i = 0;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "FP_par_arr - ADD|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i + 10] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "FP_par_arr - ADD|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i + 10] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - ADD|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i * 5] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}
}



void fp_elops_par_var_MUL(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y) {

	int i = 0;


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "FP_par_var - MUL");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*(*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - MUL|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*b)*(*c);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "FP_par_var - MUL|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*c)*(*b)*(*d);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "FP_par_var - MUL|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*b)*(*c)*(*e)*(*d)*(*f);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "FP_par_var - MUL|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*b)*(*c)*(*e)*(*d)*(*f)*(*g)*(*h)*(*j)*(*x)*(*y);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - MUL|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*c);
		(*b) = (*a)*(*b);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c);



	strcpy(op_name, "FP_par_var - MUL|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*c);
		(*b) = (*a)*(*b);
		(*d) = (*a)*(*d);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d);



	strcpy(op_name, "FP_par_var - MUL|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*c);
		(*b) = (*a)*(*b);
		(*d) = (*a)*(*d);
		(*e) = (*a)*(*e);
		(*f) = (*a)*(*f);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f);



	strcpy(op_name, "FP_par_var - MUL|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a)*(*c);
		(*b) = (*a)*(*b);
		(*d) = (*a)*(*d);
		(*e) = (*a)*(*e);
		(*f) = (*a)*(*f);
		(*g) = (*a)*(*g);
		(*h) = (*a)*(*h);
		(*j) = (*a)*(*j);
		(*x) = (*a)*(*x);
		(*y) = (*a)*(*y);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f)*(*g)*(*h)*(*j)*(*x)*(*y);




	strcpy(op_name, "FP_par_var - MUL|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - MUL|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) * 25;
		(*b) = (*b) * 75;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c);



	strcpy(op_name, "FP_par_var - MUL|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) * 25;
		(*b) = (*b) * 75;
		(*d) = (*d) * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d);



	strcpy(op_name, "FP_par_var - MUL|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) * 25;
		(*b) = (*b) * 75;
		(*d) = (*d) * 25;
		(*e) = (*e) * 75;
		(*f) = (*f) * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f);



	strcpy(op_name, "FP_par_var - MUL|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) * 25;
		(*b) = (*b) * 75;
		(*d) = (*d) * 25;
		(*e) = (*e) * 75;
		(*f) = (*f) * 25;
		(*g) = (*g) * 25;
		(*h) = (*h) * 75;
		(*j) = (*j) * 25;
		(*x) = (*x) * 75;
		(*y) = (*y) * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f)*(*g)*(*h)*(*j)*(*x)*(*y);



	strcpy(op_name, "FP_par_var - MUL|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - MUL|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*i;
		(*b) = (*b)*i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c);



	strcpy(op_name, "FP_par_var - MUL|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*i;
		(*b) = (*b)*i;
		(*d) = (*d)*i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d);



	strcpy(op_name, "FP_par_var - MUL|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*i;
		(*b) = (*b)*i;
		(*d) = (*d)*i;
		(*e) = (*e)*i;
		(*f) = (*f)*i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f);



	strcpy(op_name, "FP_par_var - MUL|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*i;
		(*b) = (*b)*i;
		(*d) = (*d)*i;
		(*e) = (*e)*i;
		(*f) = (*f)*i;
		(*g) = (*g)*i;
		(*h) = (*h)*i;
		(*j) = (*j)*i;
		(*x) = (*x)*i;
		(*y) = (*y)*i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f)*(*g)*(*h)*(*j)*(*x)*(*y);



	strcpy(op_name, "FP_par_var - MUL|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "FP_par_var - MUL|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*g_a;
		(*b) = (*b)*g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c);



	strcpy(op_name, "FP_par_var - MUL|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*g_a;
		(*b) = (*b)*g_a;
		(*d) = (*d)*g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d);



	strcpy(op_name, "FP_par_var - MUL|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*g_a;
		(*b) = (*b)*g_a;
		(*d) = (*d)*g_a;
		(*e) = (*e)*g_a;
		(*f) = (*f)*g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f);



	strcpy(op_name, "FP_par_var - MUL|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c)*g_a;
		(*b) = (*b)*g_a;
		(*d) = (*d)*g_a;
		(*e) = (*e)*g_a;
		(*f) = (*f)*g_a;
		(*g) = (*g)*g_a;
		(*h) = (*h)*g_a;
		(*j) = (*j)*g_a;
		(*x) = (*x)*g_a;
		(*y) = (*y)*g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b)*(*c)*(*d)*(*e)*(*f)*(*g)*(*h)*(*j)*(*x)*(*y);

}

void fp_elops_par_arr_MUL_l1(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y, double *z) {

	int i = 0;
	double lf[1000];


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * g[i] * h[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * g[i] * h[i] * j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * g[i] * h[i] * j[i] * x[i] * y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] * h[i];
		g[i] = f[i] * g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i];
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] * h[i];
		g[i] = f[i] * g[i];
		j[i] = f[i] * j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i];
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] * h[i];
		g[i] = f[i] * g[i];
		j[i] = f[i] * j[i];
		x[i] = f[i] * x[i];
		y[i] = f[i] * y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i];
	};



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] * h[i];
		g[i] = f[i] * g[i];
		j[i] = f[i] * j[i];
		x[i] = f[i] * x[i];
		y[i] = f[i] * y[i];
		b[i] = f[i] * b[i];
		c[i] = f[i] * c[i];
		d[i] = f[i] * d[i];
		e[i] = f[i] * e[i];
		z[i] = f[i] * z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * 25;
		g[i] = g[i] * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * 25;
		g[i] = g[i] * 25;
		j[i] = j[i] * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * 25;
		g[i] = g[i] * 25;
		j[i] = j[i] * 25;
		x[i] = x[i] * 25;
		y[i] = y[i] * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * 25;
		g[i] = g[i] * 25;
		j[i] = j[i] * 25;
		x[i] = x[i] * 25;
		y[i] = y[i] * 25;
		b[i] = b[i] * 25;
		c[i] = c[i] * 25;
		d[i] = d[i] * 25;
		e[i] = e[i] * 25;
		z[i] = z[i] * 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * (*a);
		g[i] = g[i] * (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i];
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * (*a);
		g[i] = g[i] * (*a);
		j[i] = j[i] * (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * (*a);
		g[i] = g[i] * (*a);
		j[i] = j[i] * (*a);
		x[i] = x[i] * (*a);
		y[i] = y[i] * (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * (*a);
		g[i] = g[i] * (*a);
		j[i] = j[i] * (*a);
		x[i] = x[i] * (*a);
		y[i] = y[i] * (*a);
		b[i] = b[i] * (*a);
		c[i] = c[i] * (*a);
		d[i] = d[i] * (*a);
		e[i] = e[i] * (*a);
		z[i] = z[i] * (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * i;
		g[i] = g[i] * i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i];
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * i;
		g[i] = g[i] * i;
		j[i] = j[i] * i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * i;
		g[i] = g[i] * i;
		j[i] = j[i] * i;
		x[i] = x[i] * i;
		y[i] = y[i] * i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * i;
		g[i] = g[i] * i;
		j[i] = j[i] * i;
		x[i] = x[i] * i;
		y[i] = y[i] * i;
		b[i] = b[i] * i;
		c[i] = c[i] * i;
		d[i] = d[i] * i;
		e[i] = e[i] * i;
		z[i] = z[i] * i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * lf[i];
		g[i] = g[i] * lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i];
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * lf[i];
		g[i] = g[i] * lf[i];
		j[i] = j[i] * lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * lf[i];
		g[i] = g[i] * lf[i];
		j[i] = j[i] * lf[i];
		x[i] = x[i] * lf[i];
		y[i] = y[i] * lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * lf[i];
		g[i] = g[i] * lf[i];
		j[i] = j[i] * lf[i];
		x[i] = x[i] * lf[i];
		y[i] = y[i] * lf[i];
		b[i] = b[i] * lf[i];
		c[i] = c[i] * lf[i];
		d[i] = d[i] * lf[i];
		e[i] = e[i] * lf[i];
		z[i] = z[i] * lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * g_a;
		g[i] = g[i] * g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i];
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * g_a;
		g[i] = g[i] * g_a;
		j[i] = j[i] * g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * g_a;
		g[i] = g[i] * g_a;
		j[i] = j[i] * g_a;
		x[i] = x[i] * g_a;
		y[i] = y[i] * g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * g_a;
		g[i] = g[i] * g_a;
		j[i] = j[i] * g_a;
		x[i] = x[i] * g_a;
		y[i] = y[i] * g_a;
		b[i] = b[i] * g_a;
		c[i] = c[i] * g_a;
		d[i] = d[i] * g_a;
		e[i] = e[i] * g_a;
		z[i] = z[i] * g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] * gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * gf_f[i];
		g[i] = g[i] * gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i];
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * gf_f[i];
		g[i] = g[i] * gf_f[i];
		j[i] = j[i] * gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * gf_f[i];
		g[i] = g[i] * gf_f[i];
		j[i] = j[i] * gf_f[i];
		x[i] = x[i] * gf_f[i];
		y[i] = y[i] * gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=1|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] * gf_f[i];
		g[i] = g[i] * gf_f[i];
		j[i] = j[i] * gf_f[i];
		x[i] = x[i] * gf_f[i];
		y[i] = y[i] * gf_f[i];
		b[i] = b[i] * gf_f[i];
		c[i] = c[i] * gf_f[i];
		d[i] = d[i] * gf_f[i];
		e[i] = e[i] * gf_f[i];
		z[i] = z[i] * gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] * h[i] * j[i] * x[i] * y[i] * b[i] * c[i] * d[i] * e[i] * z[i];
	}

}

void fp_elops_par_arr_MUL_l2(double *a, double b[30][30], double c[30][30], double d[30][30], double e[30][30], double f[30][30], double g[30][30], double h[30][30], double j[30][30], double x[30][30], double y[30][30], double z[30][30]) {

	int i = 0, k = 0;
	double lf[30][30];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * g[i][k] * h[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * g[i][k] * h[i][k] * j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] * h[i][k];
			g[i][k] = f[i][k] * g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] * h[i][k];
			g[i][k] = f[i][k] * g[i][k];
			j[i][k] = f[i][k] * j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] * h[i][k];
			g[i][k] = f[i][k] * g[i][k];
			j[i][k] = f[i][k] * j[i][k];
			x[i][k] = f[i][k] * x[i][k];
			y[i][k] = f[i][k] * y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	};



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] * h[i][k];
			g[i][k] = f[i][k] * g[i][k];
			j[i][k] = f[i][k] * j[i][k];
			x[i][k] = f[i][k] * x[i][k];
			y[i][k] = f[i][k] * y[i][k];
			b[i][k] = f[i][k] * b[i][k];
			c[i][k] = f[i][k] * c[i][k];
			d[i][k] = f[i][k] * d[i][k];
			e[i][k] = f[i][k] * e[i][k];
			z[i][k] = f[i][k] * z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * 25;
			g[i][k] = g[i][k] * 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * 25;
			g[i][k] = g[i][k] * 25;
			j[i][k] = j[i][k] * 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * 25;
			g[i][k] = g[i][k] * 25;
			j[i][k] = j[i][k] * 25;
			x[i][k] = x[i][k] * 25;
			y[i][k] = y[i][k] * 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * 25;
			g[i][k] = g[i][k] * 25;
			j[i][k] = j[i][k] * 25;
			x[i][k] = x[i][k] * 25;
			y[i][k] = y[i][k] * 25;
			b[i][k] = b[i][k] * 25;
			c[i][k] = c[i][k] * 25;
			d[i][k] = d[i][k] * 25;
			e[i][k] = e[i][k] * 25;
			z[i][k] = z[i][k] * 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * (*a);
			g[i][k] = g[i][k] * (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * (*a);
			g[i][k] = g[i][k] * (*a);
			j[i][k] = j[i][k] * (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * (*a);
			g[i][k] = g[i][k] * (*a);
			j[i][k] = j[i][k] * (*a);
			x[i][k] = x[i][k] * (*a);
			y[i][k] = y[i][k] * (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * (*a);
			g[i][k] = g[i][k] * (*a);
			j[i][k] = j[i][k] * (*a);
			x[i][k] = x[i][k] * (*a);
			y[i][k] = y[i][k] * (*a);
			b[i][k] = b[i][k] * (*a);
			c[i][k] = c[i][k] * (*a);
			d[i][k] = d[i][k] * (*a);
			e[i][k] = e[i][k] * (*a);
			z[i][k] = z[i][k] * (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * i;
			g[i][k] = g[i][k] * i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * i;
			g[i][k] = g[i][k] * i;
			j[i][k] = j[i][k] * i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * i;
			g[i][k] = g[i][k] * i;
			j[i][k] = j[i][k] * i;
			x[i][k] = x[i][k] * i;
			y[i][k] = y[i][k] * i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * i;
			g[i][k] = g[i][k] * i;
			j[i][k] = j[i][k] * i;
			x[i][k] = x[i][k] * i;
			y[i][k] = y[i][k] * i;
			b[i][k] = b[i][k] * i;
			c[i][k] = c[i][k] * i;
			d[i][k] = d[i][k] * i;
			e[i][k] = e[i][k] * i;
			z[i][k] = z[i][k] * i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * lf[i][k];
			g[i][k] = g[i][k] * lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * lf[i][k];
			g[i][k] = g[i][k] * lf[i][k];
			j[i][k] = j[i][k] * lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * lf[i][k];
			g[i][k] = g[i][k] * lf[i][k];
			j[i][k] = j[i][k] * lf[i][k];
			x[i][k] = x[i][k] * lf[i][k];
			y[i][k] = y[i][k] * lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * lf[i][k];
			g[i][k] = g[i][k] * lf[i][k];
			j[i][k] = j[i][k] * lf[i][k];
			x[i][k] = x[i][k] * lf[i][k];
			y[i][k] = y[i][k] * lf[i][k];
			b[i][k] = b[i][k] * lf[i][k];
			c[i][k] = c[i][k] * lf[i][k];
			d[i][k] = d[i][k] * lf[i][k];
			e[i][k] = e[i][k] * lf[i][k];
			z[i][k] = z[i][k] * lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * g_a;
			g[i][k] = g[i][k] * g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * g_a;
			g[i][k] = g[i][k] * g_a;
			j[i][k] = j[i][k] * g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * g_a;
			g[i][k] = g[i][k] * g_a;
			j[i][k] = j[i][k] * g_a;
			x[i][k] = x[i][k] * g_a;
			y[i][k] = y[i][k] * g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * g_a;
			g[i][k] = g[i][k] * g_a;
			j[i][k] = j[i][k] * g_a;
			x[i][k] = x[i][k] * g_a;
			y[i][k] = y[i][k] * g_a;
			b[i][k] = b[i][k] * g_a;
			c[i][k] = c[i][k] * g_a;
			d[i][k] = d[i][k] * g_a;
			e[i][k] = e[i][k] * g_a;
			z[i][k] = z[i][k] * g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] * l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * l2_gf_f[i][k];
			g[i][k] = g[i][k] * l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k];
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * l2_gf_f[i][k];
			g[i][k] = g[i][k] * l2_gf_f[i][k];
			j[i][k] = j[i][k] * l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * l2_gf_f[i][k];
			g[i][k] = g[i][k] * l2_gf_f[i][k];
			j[i][k] = j[i][k] * l2_gf_f[i][k];
			x[i][k] = x[i][k] * l2_gf_f[i][k];
			y[i][k] = y[i][k] * l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k];
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=2|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] * l2_gf_f[i][k];
			g[i][k] = g[i][k] * l2_gf_f[i][k];
			j[i][k] = j[i][k] * l2_gf_f[i][k];
			x[i][k] = x[i][k] * l2_gf_f[i][k];
			y[i][k] = y[i][k] * l2_gf_f[i][k];
			b[i][k] = b[i][k] * l2_gf_f[i][k];
			c[i][k] = c[i][k] * l2_gf_f[i][k];
			d[i][k] = d[i][k] * l2_gf_f[i][k];
			e[i][k] = e[i][k] * l2_gf_f[i][k];
			z[i][k] = z[i][k] * l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] * h[i][k] * j[i][k] * x[i][k] * y[i][k] * b[i][k] * c[i][k] * d[i][k] * e[i][k] * z[i][k];
		}
	}

}


void fp_elops_par_arr_MUL_l3(double *a, double b[10][10][10], double c[10][10][10], double d[10][10][10], double e[10][10][10], double f[10][10][10], double g[10][10][10], double h[10][10][10], double j[10][10][10], double x[10][10][10], double y[10][10][10], double z[10][10][10]) {


int i = 0, k = 0, n = 0;
double lf[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;


	timer_setup();


	int *mem_loc = 0; //define according to platform

	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * g[i][k][n] * h[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] * h[i][k][n];
				g[i][k][n] = f[i][k][n] * g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n];
			}
		}
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] * h[i][k][n];
				g[i][k][n] = f[i][k][n] * g[i][k][n];
				j[i][k][n] = f[i][k][n] * j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}




	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] * h[i][k][n];
				g[i][k][n] = f[i][k][n] * g[i][k][n];
				j[i][k][n] = f[i][k][n] * j[i][k][n];
				x[i][k][n] = f[i][k][n] * x[i][k][n];
				y[i][k][n] = f[i][k][n] * y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	};



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] * h[i][k][n];
				g[i][k][n] = f[i][k][n] * g[i][k][n];
				j[i][k][n] = f[i][k][n] * j[i][k][n];
				x[i][k][n] = f[i][k][n] * x[i][k][n];
				y[i][k][n] = f[i][k][n] * y[i][k][n];
				b[i][k][n] = f[i][k][n] * b[i][k][n];
				c[i][k][n] = f[i][k][n] * c[i][k][n];
				d[i][k][n] = f[i][k][n] * d[i][k][n];
				e[i][k][n] = f[i][k][n] * e[i][k][n];
				z[i][k][n] = f[i][k][n] * z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * 25;
				g[i][k][n] = g[i][k][n] * 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * 25;
				g[i][k][n] = g[i][k][n] * 25;
				j[i][k][n] = j[i][k][n] * 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * 25;
				g[i][k][n] = g[i][k][n] * 25;
				j[i][k][n] = j[i][k][n] * 25;
				x[i][k][n] = x[i][k][n] * 25;
				y[i][k][n] = y[i][k][n] * 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * 25;
				g[i][k][n] = g[i][k][n] * 25;
				j[i][k][n] = j[i][k][n] * 25;
				x[i][k][n] = x[i][k][n] * 25;
				y[i][k][n] = y[i][k][n] * 25;
				b[i][k][n] = b[i][k][n] * 25;
				c[i][k][n] = c[i][k][n] * 25;
				d[i][k][n] = d[i][k][n] * 25;
				e[i][k][n] = e[i][k][n] * 25;
				z[i][k][n] = z[i][k][n] * 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * (*a);
				g[i][k][n] = g[i][k][n] * (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * (*a);
				g[i][k][n] = g[i][k][n] * (*a);
				j[i][k][n] = j[i][k][n] * (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * (*a);
				g[i][k][n] = g[i][k][n] * (*a);
				j[i][k][n] = j[i][k][n] * (*a);
				x[i][k][n] = x[i][k][n] * (*a);
				y[i][k][n] = y[i][k][n] * (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * (*a);
				g[i][k][n] = g[i][k][n] * (*a);
				j[i][k][n] = j[i][k][n] * (*a);
				x[i][k][n] = x[i][k][n] * (*a);
				y[i][k][n] = y[i][k][n] * (*a);
				b[i][k][n] = b[i][k][n] * (*a);
				c[i][k][n] = c[i][k][n] * (*a);
				d[i][k][n] = d[i][k][n] * (*a);
				e[i][k][n] = e[i][k][n] * (*a);
				z[i][k][n] = z[i][k][n] * (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * i;
				g[i][k][n] = g[i][k][n] * i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * i;
				g[i][k][n] = g[i][k][n] * i;
				j[i][k][n] = j[i][k][n] * i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * i;
				g[i][k][n] = g[i][k][n] * i;
				j[i][k][n] = j[i][k][n] * i;
				x[i][k][n] = x[i][k][n] * i;
				y[i][k][n] = y[i][k][n] * i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * i;
				g[i][k][n] = g[i][k][n] * i;
				j[i][k][n] = j[i][k][n] * i;
				x[i][k][n] = x[i][k][n] * i;
				y[i][k][n] = y[i][k][n] * i;
				b[i][k][n] = b[i][k][n] * i;
				c[i][k][n] = c[i][k][n] * i;
				d[i][k][n] = d[i][k][n] * i;
				e[i][k][n] = e[i][k][n] * i;
				z[i][k][n] = z[i][k][n] * i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * lf[i][k][n];
				g[i][k][n] = g[i][k][n] * lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * lf[i][k][n];
				g[i][k][n] = g[i][k][n] * lf[i][k][n];
				j[i][k][n] = j[i][k][n] * lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * lf[i][k][n];
				g[i][k][n] = g[i][k][n] * lf[i][k][n];
				j[i][k][n] = j[i][k][n] * lf[i][k][n];
				x[i][k][n] = x[i][k][n] * lf[i][k][n];
				y[i][k][n] = y[i][k][n] * lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * lf[i][k][n];
				g[i][k][n] = g[i][k][n] * lf[i][k][n];
				j[i][k][n] = j[i][k][n] * lf[i][k][n];
				x[i][k][n] = x[i][k][n] * lf[i][k][n];
				y[i][k][n] = y[i][k][n] * lf[i][k][n];
				b[i][k][n] = b[i][k][n] * lf[i][k][n];
				c[i][k][n] = c[i][k][n] * lf[i][k][n];
				d[i][k][n] = d[i][k][n] * lf[i][k][n];
				e[i][k][n] = e[i][k][n] * lf[i][k][n];
				z[i][k][n] = z[i][k][n] * lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * g_a;
				g[i][k][n] = g[i][k][n] * g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * g_a;
				g[i][k][n] = g[i][k][n] * g_a;
				j[i][k][n] = j[i][k][n] * g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * g_a;
				g[i][k][n] = g[i][k][n] * g_a;
				j[i][k][n] = j[i][k][n] * g_a;
				x[i][k][n] = x[i][k][n] * g_a;
				y[i][k][n] = y[i][k][n] * g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * g_a;
				g[i][k][n] = g[i][k][n] * g_a;
				j[i][k][n] = j[i][k][n] * g_a;
				x[i][k][n] = x[i][k][n] * g_a;
				y[i][k][n] = y[i][k][n] * g_a;
				b[i][k][n] = b[i][k][n] * g_a;
				c[i][k][n] = c[i][k][n] * g_a;
				d[i][k][n] = d[i][k][n] * g_a;
				e[i][k][n] = e[i][k][n] * g_a;
				z[i][k][n] = z[i][k][n] * g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] * l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] * l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n];
			}
		}
	}


	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] * l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] * l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] * l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] * l3_gf_f[i][k][n];
				x[i][k][n] = x[i][k][n] * l3_gf_f[i][k][n];
				y[i][k][n] = y[i][k][n] * l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n];
			}
		}
	}



	strcpy(op_name, "FP_par_arr - MUL|simple|lvl=3|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] * l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] * l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] * l3_gf_f[i][k][n];
				x[i][k][n] = x[i][k][n] * l3_gf_f[i][k][n];
				y[i][k][n] = y[i][k][n] * l3_gf_f[i][k][n];
				b[i][k][n] = b[i][k][n] * l3_gf_f[i][k][n];
				c[i][k][n] = c[i][k][n] * l3_gf_f[i][k][n];
				d[i][k][n] = d[i][k][n] * l3_gf_f[i][k][n];
				e[i][k][n] = e[i][k][n] * l3_gf_f[i][k][n];
				z[i][k][n] = z[i][k][n] * l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] * h[i][k][n] * j[i][k][n] * x[i][k][n] * y[i][k][n] * b[i][k][n] * c[i][k][n] * d[i][k][n] * e[i][k][n] * z[i][k][n];
			}
		}
	}


}

void fp_elops_par_arr_MUL_cmplx(double *f, double *g) {


	int i = 0;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "FP_par_arr - MUL|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i + 10] * g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10] * g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "FP_par_arr - MUL|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i] * g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i + 10] * g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "FP_par_arr - MUL|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i * 5] * g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}
}


void fp_elops_par_var_DIV(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y) {

	int i = 0;
	double lb = 678;


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "INT_par_var - DIV");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "INT_par_var - DIV|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*b) / (*c);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "INT_par_var - DIV|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*c) / (*b) / (*d);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "INT_par_var - DIV|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*b) / (*c) / (*e) / (*d) / (*f);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "INT_par_var - DIV|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*b) / (*c) / (*e) / (*d) / (*f) / (*g) / (*h) / (*j) / (*x) / (*y);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "INT_par_var - DIV|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*c);
		(*b) = (*a) / (*b);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c);



	strcpy(op_name, "INT_par_var - DIV|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*c);
		(*b) = (*a) / (*b);
		(*d) = (*a) / (*d);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d);



	strcpy(op_name, "INT_par_var - DIV|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*c);
		(*b) = (*a) / (*b);
		(*d) = (*a) / (*d);
		(*e) = (*a) / (*e);
		(*f) = (*a) / (*f);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f);



	strcpy(op_name, "INT_par_var - DIV|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) / (*c);
		(*b) = (*a) / (*b);
		(*d) = (*a) / (*d);
		(*e) = (*a) / (*e);
		(*f) = (*a) / (*f);
		(*g) = (*a) / (*g);
		(*h) = (*a) / (*h);
		(*j) = (*a) / (*j);
		(*x) = (*a) / (*x);
		(*y) = (*a) / (*y);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f) / (*g) / (*h) / (*j) / (*x) / (*y);




	strcpy(op_name, "INT_par_var - DIV|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "INT_par_var - DIV|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / 25;
		(*b) = (*b) / 75;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c);



	strcpy(op_name, "INT_par_var - DIV|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / 25;
		(*b) = (*b) / 75;
		(*d) = (*d) / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d);



	strcpy(op_name, "INT_par_var - DIV|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / 25;
		(*b) = (*b) / 75;
		(*d) = (*d) / 25;
		(*e) = (*e) / 75;
		(*f) = (*f) / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f);



	strcpy(op_name, "INT_par_var - DIV|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / 25;
		(*b) = (*b) / 75;
		(*d) = (*d) / 25;
		(*e) = (*e) / 75;
		(*f) = (*f) / 25;
		(*g) = (*g) / 25;
		(*h) = (*h) / 75;
		(*j) = (*j) / 25;
		(*x) = (*x) / 75;
		(*y) = (*y) / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f) / (*g) / (*h) / (*j) / (*x) / (*y);



	strcpy(op_name, "INT_par_var - DIV|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "INT_par_var - DIV|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / lb;
		(*b) = (*b) / lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c);



	strcpy(op_name, "INT_par_var - DIV|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) /lb;
		(*b) = (*b) /lb;
		(*d) = (*d) /lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d);



	strcpy(op_name, "INT_par_var - DIV|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) /lb;
		(*b) = (*b) /lb;
		(*d) = (*d) /lb;
		(*e) = (*e) /lb;
		(*f) = (*f) /lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f);



	strcpy(op_name, "INT_par_var - DIV|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) /lb;
		(*b) = (*b) /lb;
		(*d) = (*d) /lb;
		(*e) = (*e) /lb;
		(*f) = (*f) /lb;
		(*g) = (*g) /lb;
		(*h) = (*h) /lb;
		(*j) = (*j) /lb;
		(*x) = (*x) /lb;
		(*y) = (*y) /lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f) / (*g) / (*h) / (*j) / (*x) / (*y);



	strcpy(op_name, "INT_par_var - DIV|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "INT_par_var - DIV|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / g_a;
		(*b) = (*b) / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c);



	strcpy(op_name, "INT_par_var - DIV|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / g_a;
		(*b) = (*b) / g_a;
		(*d) = (*d) / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d);



	strcpy(op_name, "INT_par_var - DIV|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / g_a;
		(*b) = (*b) / g_a;
		(*d) = (*d) / g_a;
		(*e) = (*e) / g_a;
		(*f) = (*f) / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f);



	strcpy(op_name, "INT_par_var - DIV|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*c) / g_a;
		(*b) = (*b) / g_a;
		(*d) = (*d) / g_a;
		(*e) = (*e) / g_a;
		(*f) = (*f) / g_a;
		(*g) = (*g) / g_a;
		(*h) = (*h) / g_a;
		(*j) = (*j) / g_a;
		(*x) = (*x) / g_a;
		(*y) = (*y) / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) / (*c) / (*d) / (*e) / (*f) / (*g) / (*h) / (*j) / (*x) / (*y);

}

void fp_elops_par_arr_DIV_l1(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y, double *z) {

	int i = 0;
	double lb = 678, lf[1000];


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / g[i] / h[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / g[i] / h[i] / j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / g[i] / h[i] / j[i] / x[i] / y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / g[i] / h[i] / j[i] / x[i] / y[i] / b[i] / c[i] / d[i] / e[i] / z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  h[i]/f[i];
		g[i] = g[i]/f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] +h[i];
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  h[i]/f[i];
		g[i] = g[i]/f[i];
		j[i] = j[i] / f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  h[i]/f[i];
		g[i] = g[i]/f[i];
		j[i] = j[i] / f[i];
		x[i] = x[i] / f[i];
		y[i] = y[i] / f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	};



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  h[i]/f[i];
		g[i] = g[i]/f[i];
		j[i] = j[i] / f[i];
		x[i] = x[i] / f[i];
		y[i] = y[i] / f[i];
		b[i] = b[i] / f[i];
		c[i] = c[i] / f[i];
		d[i] = d[i] / f[i];
		e[i] = e[i] / f[i];
		z[i] = z[i] / f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / 25;
		g[i] = g[i] / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] +h[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / 25;
		g[i] = g[i] / 25;
		j[i] = j[i] / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / 25;
		g[i] = g[i] / 25;
		j[i] = j[i] / 25;
		x[i] = x[i] / 25;
		y[i] = y[i] / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / 25;
		g[i] = g[i] / 25;
		j[i] = j[i] / 25;
		x[i] = x[i] / 25;
		y[i] = y[i] / 25;
		b[i] = b[i] / 25;
		c[i] = c[i] / 25;
		d[i] = d[i] / 25;
		e[i] = e[i] / 25;
		z[i] = z[i] / 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / (*a);
		g[i] = g[i] / (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] +h[i];
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / (*a);
		g[i] = g[i] / (*a);
		j[i] = j[i] / (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / (*a);
		g[i] = g[i] / (*a);
		j[i] = j[i] / (*a);
		x[i] = x[i] / (*a);
		y[i] = y[i] / (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / (*a);
		g[i] = g[i] / (*a);
		j[i] = j[i] / (*a);
		x[i] = x[i] / (*a);
		y[i] = y[i] / (*a);
		b[i] = b[i] / (*a);
		c[i] = c[i] / (*a);
		d[i] = d[i] / (*a);
		e[i] = e[i] / (*a);
		z[i] = z[i] / (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lb;
		g[i] = g[i] / lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] +h[i];
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lb;
		g[i] = g[i] / lb;
		j[i] = j[i] / lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lb;
		g[i] = g[i] / lb;
		j[i] = j[i] / lb;
		x[i] = x[i] / lb;
		y[i] = y[i] / lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lb;
		g[i] = g[i] / lb;
		j[i] = j[i] / lb;
		x[i] = x[i] / lb;
		y[i] = y[i] / lb;
		b[i] = b[i] / lb;
		c[i] = c[i] / lb;
		d[i] = d[i] / lb;
		e[i] = e[i] / lb;
		z[i] = z[i] / lb;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lf[i];
		g[i] = g[i] / lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] +h[i];
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lf[i];
		g[i] = g[i] / lf[i];
		j[i] = j[i] / lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lf[i];
		g[i] = g[i] / lf[i];
		j[i] = j[i] / lf[i];
		x[i] = x[i] / lf[i];
		y[i] = y[i] / lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / lf[i];
		g[i] = g[i] / lf[i];
		j[i] = j[i] / lf[i];
		x[i] = x[i] / lf[i];
		y[i] = y[i] / lf[i];
		b[i] = b[i] / lf[i];
		c[i] = c[i] / lf[i];
		d[i] = d[i] / lf[i];
		e[i] = e[i] / lf[i];
		z[i] = z[i] / lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / g_a;
		g[i] = g[i] / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] +h[i];
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / g_a;
		g[i] = g[i] / g_a;
		j[i] = j[i] / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / g_a;
		g[i] = g[i] / g_a;
		j[i] = j[i] / g_a;
		x[i] = x[i] / g_a;
		y[i] = y[i] / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / g_a;
		g[i] = g[i] / g_a;
		j[i] = j[i] / g_a;
		x[i] = x[i] / g_a;
		y[i] = y[i] / g_a;
		b[i] = b[i] / g_a;
		c[i] = c[i] / g_a;
		d[i] = d[i] / g_a;
		e[i] = e[i] / g_a;
		z[i] = z[i] / g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] / gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / gf_f[i];
		g[i] = g[i] / gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] +h[i];
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / gf_f[i];
		g[i] = g[i] / gf_f[i];
		j[i] = j[i] / gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / gf_f[i];
		g[i] = g[i] / gf_f[i];
		j[i] = j[i] / gf_f[i];
		x[i] = x[i] / gf_f[i];
		y[i] = y[i] / gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=1|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] / gf_f[i];
		g[i] = g[i] / gf_f[i];
		j[i] = j[i] / gf_f[i];
		x[i] = x[i] / gf_f[i];
		y[i] = y[i] / gf_f[i];
		b[i] = b[i] / gf_f[i];
		c[i] = c[i] / gf_f[i];
		d[i] = d[i] / gf_f[i];
		e[i] = e[i] / gf_f[i];
		z[i] = z[i] / gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}

}

void fp_elops_par_arr_DIV_l2(double *a, double b[30][30], double c[30][30], double d[30][30], double e[30][30], double f[30][30], double g[30][30], double h[30][30], double j[30][30], double x[30][30], double y[30][30], double z[30][30]) {

	int i = 0, k = 0;
	double lb = 789, lf[30][30];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / g[i][k] / h[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / g[i][k] / h[i][k] / j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / g[i][k] / h[i][k] / j[i][k] / x[i][k] / y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / g[i][k] / h[i][k] / j[i][k] / x[i][k] / y[i][k] / b[i][k] / c[i][k] / d[i][k] / e[i][k] / z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] / h[i][k];
			g[i][k] = f[i][k] / g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k]+ h[i][k];
		}
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] / h[i][k];
			g[i][k] = f[i][k] / g[i][k];
			j[i][k] = f[i][k] / j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] / h[i][k];
			g[i][k] = f[i][k] / g[i][k];
			j[i][k] = f[i][k] / j[i][k];
			x[i][k] = f[i][k] / x[i][k];
			y[i][k] = f[i][k] / y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k]+y[i][k];
		}
	};



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] / h[i][k];
			g[i][k] = f[i][k] / g[i][k];
			j[i][k] = f[i][k] / j[i][k];
			x[i][k] = f[i][k] / x[i][k];
			y[i][k] = f[i][k] / y[i][k];
			b[i][k] = f[i][k] / b[i][k];
			c[i][k] = f[i][k] / c[i][k];
			d[i][k] = f[i][k] / d[i][k];
			e[i][k] = f[i][k] / e[i][k];
			z[i][k] = f[i][k] / z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / 25;
			g[i][k] = g[i][k] / 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k]+ h[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / 25;
			g[i][k] = g[i][k] / 25;
			j[i][k] = j[i][k] / 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / 25;
			g[i][k] = g[i][k] / 25;
			j[i][k] = j[i][k] / 25;
			x[i][k] = x[i][k] / 25;
			y[i][k] = y[i][k] / 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k]+y[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / 25;
			g[i][k] = g[i][k] / 25;
			j[i][k] = j[i][k] / 25;
			x[i][k] = x[i][k] / 25;
			y[i][k] = y[i][k] / 25;
			b[i][k] = b[i][k] / 25;
			c[i][k] = c[i][k] / 25;
			d[i][k] = d[i][k] / 25;
			e[i][k] = e[i][k] / 25;
			z[i][k] = z[i][k] / 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / (*a);
			g[i][k] = g[i][k] / (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k]+ h[i][k];
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / (*a);
			g[i][k] = g[i][k] / (*a);
			j[i][k] = j[i][k] / (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / (*a);
			g[i][k] = g[i][k] / (*a);
			j[i][k] = j[i][k] / (*a);
			x[i][k] = x[i][k] / (*a);
			y[i][k] = y[i][k] / (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k]+y[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / (*a);
			g[i][k] = g[i][k] / (*a);
			j[i][k] = j[i][k] / (*a);
			x[i][k] = x[i][k] / (*a);
			y[i][k] = y[i][k] / (*a);
			b[i][k] = b[i][k] / (*a);
			c[i][k] = c[i][k] / (*a);
			d[i][k] = d[i][k] / (*a);
			e[i][k] = e[i][k] / (*a);
			z[i][k] = z[i][k] / (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / lb;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lb;
			g[i][k] = g[i][k] / lb;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k]+ h[i][k];
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lb;
			g[i][k] = g[i][k] / lb;
			j[i][k] = j[i][k] / lb;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lb;
			g[i][k] = g[i][k] / lb;
			j[i][k] = j[i][k] / lb;
			x[i][k] = x[i][k] / lb;
			y[i][k] = y[i][k] / lb;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k]+y[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lb;
			g[i][k] = g[i][k] / lb;
			j[i][k] = j[i][k] / lb;
			x[i][k] = x[i][k] / lb;
			y[i][k] = y[i][k] / lb;
			b[i][k] = b[i][k] / lb;
			c[i][k] = c[i][k] / lb;
			d[i][k] = d[i][k] / lb;
			e[i][k] = e[i][k] / lb;
			z[i][k] = z[i][k] / lb;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lf[i][k];
			g[i][k] = g[i][k] / lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k]+ h[i][k];
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lf[i][k];
			g[i][k] = g[i][k] / lf[i][k];
			j[i][k] = j[i][k] / lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lf[i][k];
			g[i][k] = g[i][k] / lf[i][k];
			j[i][k] = j[i][k] / lf[i][k];
			x[i][k] = x[i][k] / lf[i][k];
			y[i][k] = y[i][k] / lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k]+y[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / lf[i][k];
			g[i][k] = g[i][k] / lf[i][k];
			j[i][k] = j[i][k] / lf[i][k];
			x[i][k] = x[i][k] / lf[i][k];
			y[i][k] = y[i][k] / lf[i][k];
			b[i][k] = b[i][k] / lf[i][k];
			c[i][k] = c[i][k] / lf[i][k];
			d[i][k] = d[i][k] / lf[i][k];
			e[i][k] = e[i][k] / lf[i][k];
			z[i][k] = z[i][k] / lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / g_a;
			g[i][k] = g[i][k] / g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k]+ h[i][k];
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / g_a;
			g[i][k] = g[i][k] / g_a;
			j[i][k] = j[i][k] / g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / g_a;
			g[i][k] = g[i][k] / g_a;
			j[i][k] = j[i][k] / g_a;
			x[i][k] = x[i][k] / g_a;
			y[i][k] = y[i][k] / g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k]+y[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / g_a;
			g[i][k] = g[i][k] / g_a;
			j[i][k] = j[i][k] / g_a;
			x[i][k] = x[i][k] / g_a;
			y[i][k] = y[i][k] / g_a;
			b[i][k] = b[i][k] / g_a;
			c[i][k] = c[i][k] / g_a;
			d[i][k] = d[i][k] / g_a;
			e[i][k] = e[i][k] / g_a;
			z[i][k] = z[i][k] / g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] / l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / l2_gf_f[i][k];
			g[i][k] = g[i][k] / l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k]+ h[i][k];
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / l2_gf_f[i][k];
			g[i][k] = g[i][k] / l2_gf_f[i][k];
			j[i][k] = j[i][k] / l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / l2_gf_f[i][k];
			g[i][k] = g[i][k] / l2_gf_f[i][k];
			j[i][k] = j[i][k] / l2_gf_f[i][k];
			x[i][k] = x[i][k] / l2_gf_f[i][k];
			y[i][k] = y[i][k] / l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k]+y[i][k];
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=2|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] / l2_gf_f[i][k];
			g[i][k] = g[i][k] / l2_gf_f[i][k];
			j[i][k] = j[i][k] / l2_gf_f[i][k];
			x[i][k] = x[i][k] / l2_gf_f[i][k];
			y[i][k] = y[i][k] / l2_gf_f[i][k];
			b[i][k] = b[i][k] / l2_gf_f[i][k];
			c[i][k] = c[i][k] / l2_gf_f[i][k];
			d[i][k] = d[i][k] / l2_gf_f[i][k];
			e[i][k] = e[i][k] / l2_gf_f[i][k];
			z[i][k] = z[i][k] / l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}

}


void fp_elops_par_arr_DIV_l3(double *a, double b[10][10][10], double c[10][10][10], double d[10][10][10], double e[10][10][10], double f[10][10][10], double g[10][10][10], double h[10][10][10], double j[10][10][10], double x[10][10][10], double y[10][10][10], double z[10][10][10]) {


	int i = 0, k = 0, n = 0;
	double lb = 798, lf[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / g[i][k][n] / h[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / g[i][k][n] / h[i][k][n] / j[i][k][n] / x[i][k][n] / y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / g[i][k][n] / h[i][k][n] / j[i][k][n] / x[i][k][n] / y[i][k][n] / b[i][k][n] / c[i][k][n] / d[i][k][n] / e[i][k][n] / z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] / h[i][k][n];
				g[i][k][n] = f[i][k][n] / g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n];
			}
		}
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] / h[i][k][n];
				g[i][k][n] = f[i][k][n] / g[i][k][n];
				j[i][k][n] = f[i][k][n] / j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}




	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] / h[i][k][n];
				g[i][k][n] = f[i][k][n] / g[i][k][n];
				j[i][k][n] = f[i][k][n] / j[i][k][n];
				x[i][k][n] = f[i][k][n] / x[i][k][n];
				y[i][k][n] = f[i][k][n] / y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] +  h[i][k][n] +  j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	};



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] / h[i][k][n];
				g[i][k][n] = f[i][k][n] / g[i][k][n];
				j[i][k][n] = f[i][k][n] / j[i][k][n];
				x[i][k][n] = f[i][k][n] / x[i][k][n];
				y[i][k][n] = f[i][k][n] / y[i][k][n];
				b[i][k][n] = f[i][k][n] / b[i][k][n];
				c[i][k][n] = f[i][k][n] / c[i][k][n];
				d[i][k][n] = f[i][k][n] / d[i][k][n];
				e[i][k][n] = f[i][k][n] / e[i][k][n];
				z[i][k][n] = f[i][k][n] / z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / 25;
				g[i][k][n] = g[i][k][n] / 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / 25;
				g[i][k][n] = g[i][k][n] / 25;
				j[i][k][n] = j[i][k][n] / 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / 25;
				g[i][k][n] = g[i][k][n] / 25;
				j[i][k][n] = j[i][k][n] / 25;
				x[i][k][n] = x[i][k][n] / 25;
				y[i][k][n] = y[i][k][n] / 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] +  h[i][k][n] +  j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / 25;
				g[i][k][n] = g[i][k][n] / 25;
				j[i][k][n] = j[i][k][n] / 25;
				x[i][k][n] = x[i][k][n] / 25;
				y[i][k][n] = y[i][k][n] / 25;
				b[i][k][n] = b[i][k][n] / 25;
				c[i][k][n] = c[i][k][n] / 25;
				d[i][k][n] = d[i][k][n] / 25;
				e[i][k][n] = e[i][k][n] / 25;
				z[i][k][n] = z[i][k][n] / 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / (*a);
				g[i][k][n] = g[i][k][n] / (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n];
			}
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / (*a);
				g[i][k][n] = g[i][k][n] / (*a);
				j[i][k][n] = j[i][k][n] / (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / (*a);
				g[i][k][n] = g[i][k][n] / (*a);
				j[i][k][n] = j[i][k][n] / (*a);
				x[i][k][n] = x[i][k][n] / (*a);
				y[i][k][n] = y[i][k][n] / (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] +  h[i][k][n] +  j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / (*a);
				g[i][k][n] = g[i][k][n] / (*a);
				j[i][k][n] = j[i][k][n] / (*a);
				x[i][k][n] = x[i][k][n] / (*a);
				y[i][k][n] = y[i][k][n] / (*a);
				b[i][k][n] = b[i][k][n] / (*a);
				c[i][k][n] = c[i][k][n] / (*a);
				d[i][k][n] = d[i][k][n] / (*a);
				e[i][k][n] = e[i][k][n] / (*a);
				z[i][k][n] = z[i][k][n] / (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / lb;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lb;
				g[i][k][n] = g[i][k][n] / lb;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n];
			}
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lb;
				g[i][k][n] = g[i][k][n] / lb;
				j[i][k][n] = j[i][k][n] / lb;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lb;
				g[i][k][n] = g[i][k][n] / lb;
				j[i][k][n] = j[i][k][n] / lb;
				x[i][k][n] = x[i][k][n] / lb;
				y[i][k][n] = y[i][k][n] / lb;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] +  h[i][k][n] +  j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lb;
				g[i][k][n] = g[i][k][n] / lb;
				j[i][k][n] = j[i][k][n] / lb;
				x[i][k][n] = x[i][k][n] / lb;
				y[i][k][n] = y[i][k][n] / lb;
				b[i][k][n] = b[i][k][n] / lb;
				c[i][k][n] = c[i][k][n] / lb;
				d[i][k][n] = d[i][k][n] / lb;
				e[i][k][n] = e[i][k][n] / lb;
				z[i][k][n] = z[i][k][n] / lb;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lf[i][k][n];
				g[i][k][n] = g[i][k][n] / lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n];
			}
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lf[i][k][n];
				g[i][k][n] = g[i][k][n] / lf[i][k][n];
				j[i][k][n] = j[i][k][n] / lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lf[i][k][n];
				g[i][k][n] = g[i][k][n] / lf[i][k][n];
				j[i][k][n] = j[i][k][n] / lf[i][k][n];
				x[i][k][n] = x[i][k][n] / lf[i][k][n];
				y[i][k][n] = y[i][k][n] / lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] +  h[i][k][n] +  j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / lf[i][k][n];
				g[i][k][n] = g[i][k][n] / lf[i][k][n];
				j[i][k][n] = j[i][k][n] / lf[i][k][n];
				x[i][k][n] = x[i][k][n] / lf[i][k][n];
				y[i][k][n] = y[i][k][n] / lf[i][k][n];
				b[i][k][n] = b[i][k][n] / lf[i][k][n];
				c[i][k][n] = c[i][k][n] / lf[i][k][n];
				d[i][k][n] = d[i][k][n] / lf[i][k][n];
				e[i][k][n] = e[i][k][n] / lf[i][k][n];
				z[i][k][n] = z[i][k][n] / lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / g_a;
				g[i][k][n] = g[i][k][n] / g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n];
			}
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / g_a;
				g[i][k][n] = g[i][k][n] / g_a;
				j[i][k][n] = j[i][k][n] / g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / g_a;
				g[i][k][n] = g[i][k][n] / g_a;
				j[i][k][n] = j[i][k][n] / g_a;
				x[i][k][n] = x[i][k][n] / g_a;
				y[i][k][n] = y[i][k][n] / g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] +  h[i][k][n] +  j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / g_a;
				g[i][k][n] = g[i][k][n] / g_a;
				j[i][k][n] = j[i][k][n] / g_a;
				x[i][k][n] = x[i][k][n] / g_a;
				y[i][k][n] = y[i][k][n] / g_a;
				b[i][k][n] = b[i][k][n] / g_a;
				c[i][k][n] = c[i][k][n] / g_a;
				d[i][k][n] = d[i][k][n] / g_a;
				e[i][k][n] = e[i][k][n] / g_a;
				z[i][k][n] = z[i][k][n] / g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] / l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] / l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n];
			}
		}
	}


	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] / l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] / l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] / h[i][k][n] / j[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] / l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] / l3_gf_f[i][k][n];
				x[i][k][n] = x[i][k][n] / l3_gf_f[i][k][n];
				y[i][k][n] = y[i][k][n] / l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] +  h[i][k][n] +  j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "INT_par_arr - DIV|simple|lvl=3|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] / l3_gf_f[i][k][n];
				g[i][k][n] = g[i][k][n] / l3_gf_f[i][k][n];
				j[i][k][n] = j[i][k][n] / l3_gf_f[i][k][n];
				x[i][k][n] = x[i][k][n] / l3_gf_f[i][k][n];
				y[i][k][n] = y[i][k][n] / l3_gf_f[i][k][n];
				b[i][k][n] = b[i][k][n] / l3_gf_f[i][k][n];
				c[i][k][n] = c[i][k][n] / l3_gf_f[i][k][n];
				d[i][k][n] = d[i][k][n] / l3_gf_f[i][k][n];
				e[i][k][n] = e[i][k][n] / l3_gf_f[i][k][n];
				z[i][k][n] = z[i][k][n] / l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}


}

void fp_elops_par_arr_DIV_cmplx(double *f, double *g) {


	int i = 0;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "INT_par_arr - DIV|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i + 10] / g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10] / g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "INT_par_arr - DIV|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i] / g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i / +10] / g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "INT_par_arr - DIV|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i * 5] / g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}
}

