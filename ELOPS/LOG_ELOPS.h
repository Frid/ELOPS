#include <stdlib.h>
#define TICKS_PER_SEC 333333344 // set value according to platform



extern int g_a, g_b, g_c, g_d, g_e, g_f, g_g, g_h, g_j, g_x, g_y;

extern int gf_b[1000], gf_c[1000], gf_d[1000], gf_e[1000], gf_f[2000], gf_g[1000], gf_h[1000], gf_j[1000], gf_x[1000], gf_y[1000], gf_z[1000];

extern int l2_gf_b[30][30], l2_gf_c[30][30], l2_gf_d[30][30], l2_gf_e[30][30], l2_gf_f[30][30], l2_gf_g[30][30], l2_gf_h[30][30], l2_gf_j[30][30], l2_gf_x[30][30], l2_gf_y[30][30], l2_gf_z[30][30];

extern int l3_gf_b[10][10][10], l3_gf_c[10][10][10], l3_gf_d[10][10][10], l3_gf_e[10][10][10], l3_gf_f[10][10][10], l3_gf_g[10][10][10], l3_gf_h[10][10][10], l3_gf_j[10][10][10], l3_gf_x[10][10][10], l3_gf_y[10][10][10], l3_gf_z[10][10][10];

extern double volatile temp_res, temp_resf[2000];

void timer_setup(); //implement according to platform

long long get_timer_value(); //implement according to platform

void store_exec_time(int *mem_loc, double exec_time_s, char *op_name);

void log_elops_loc_var_LOG();
void log_elops_loc_arr_LOG_l1();
void log_elops_loc_arr_LOG_l2();
void log_elops_loc_arr_LOG_l3();
void log_elops_loc_arr_LOG_cmplx();
	 
void log_elops_loc_var_SHIFT();
void log_elops_loc_arr_SHIFT_l1();
void log_elops_loc_arr_SHIFT_l2();
void log_elops_loc_arr_SHIFT_l3();
void log_elops_loc_arr_SHIFT_cmplx();

void log_elops_glob_var_LOG();
void log_elops_glob_arr_LOG_l1();
void log_elops_glob_arr_LOG_l2();
void log_elops_glob_arr_LOG_l3();
void log_elops_glob_arr_LOG_cmplx();
	
void log_elops_glob_var_SHIFT();
void log_elops_glob_arr_SHIFT_l1();
void log_elops_glob_arr_SHIFT_l2();
void log_elops_glob_arr_SHIFT_l3();
void log_elops_glob_arr_SHIFT_cmplx();


void log_elops_par_var_LOG(int *a, int *b, int *c, int *d, int *e, int *f, int *g, int *h, int *j, int *x, int *y);
void log_elops_par_arr_LOG_l1(int *a, int *b, int *c, int *d, int *e, int *f, int *g, int *h, int *j, int *x, int *y, int *z);
void log_elops_par_arr_LOG_l2(int *a, int b[30][30], int c[30][30], int d[30][30], int e[30][30], int f[30][30], int g[30][30], int h[30][30], int j[30][30], int x[30][30], int y[30][30], int z[30][30]);
void log_elops_par_arr_LOG_l3(int *a, int b[10][10][10], int c[10][10][10], int d[10][10][10], int e[10][10][10], int f[10][10][10], int g[10][10][10], int h[10][10][10], int j[10][10][10], int x[10][10][10], int y[10][10][10], int z[10][10][10]);
void log_elops_par_arr_LOG_cmplx(int *f, int *g);
	
void log_elops_par_var_SHIFT(int *a, int *b, int *c, int *d, int *e, int *f, int *g, int *h, int *j, int *x, int *y);
void log_elops_par_arr_SHIFT_l1(int *a, int *b, int *c, int *d, int *e, int *f, int *g, int *h, int *j, int *x, int *y, int *z);
void log_elops_par_arr_SHIFT_l2(int *a, int b[30][30], int c[30][30], int d[30][30], int e[30][30], int f[30][30], int g[30][30], int h[30][30], int j[30][30], int x[30][30], int y[30][30], int z[30][30]);
void log_elops_par_arr_SHIFT_l3(int *a, int b[10][10][10], int c[10][10][10], int d[10][10][10], int e[10][10][10], int f[10][10][10], int g[10][10][10], int h[10][10][10], int j[10][10][10], int x[10][10][10], int y[10][10][10], int z[10][10][10]);
void log_elops_par_arr_SHIFT_cmplx(int *f, int *g);
