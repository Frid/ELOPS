#include "MEM_ELOPS.h"

void mem_elops_loc_var_ASSIGN() {

	int a = 0, b = 20, c = -78, d = 45, e = -24, f = 89, g = 56, h = 98, j = -24, x = 7819, y = 567;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "MEM_loc_var - ASSIGN");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;





	strcpy(op_name, "MEM_loc_var - ASSIGN|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a;
		b = a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c;



	strcpy(op_name, "MEM_loc_var - ASSIGN|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a;
		b = a;
		d = a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d;



	strcpy(op_name, "MEM_loc_var - ASSIGN|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a ;
		b = a ;
		d = a ;
		e = a ;
		f = a ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f;



	strcpy(op_name, "MEM_loc_var - ASSIGN|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a ;
		b = a ;
		d = a ;
		e = a ;
		f = a ;
		g = a ;
		h = a ;
		j = a ;
		x = a ;
		y = a ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f + g + h + j + x + y;




	strcpy(op_name, "MEM_loc_var - ASSIGN|const");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;




	strcpy(op_name, "MEM_loc_var - ASSIGN|const|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c =  25;
		b =  75;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c;



	strcpy(op_name, "MEM_loc_var - ASSIGN|const|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c =25;
		b =75;
		d =25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d;



	strcpy(op_name, "MEM_loc_var - ASSIGN|const|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = 25;
		b = 75;
		d = 25;
		e = 75;
		f = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f;



	strcpy(op_name, "MEM_loc_var - ASSIGN|const|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c =25;
		b =75;
		d =25;
		e =75;
		f =25;
		g =25;
		h =75;
		j =25;
		x =75;
		y =25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f + g + h + j + x + y;

}

void mem_elops_loc_arr_ASSIGN_l1() {

	int a = 0, i = 0, b[1000], c[1000], d[1000], e[1000], f[1000], g[1000], h[1000], j[1000], x[1000], y[1000], z[1000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}





	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i];
		g[i] = f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] ;
		g[i] = f[i] ;
		j[i] = f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] ;
		g[i] = f[i] ;
		j[i] = f[i] ;
		x[i] = f[i] ;
		y[i] = f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	};



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] ;
		g[i] = f[i] ;
		j[i] = f[i] ;
		x[i] = f[i] ;
		y[i] = f[i] ;
		b[i] = f[i] ;
		c[i] = f[i] ;
		d[i] = f[i] ;
		e[i] = f[i] ;
		z[i] = f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  i;
		g[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  i;
		g[i] =  i;
		j[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = i;
		g[i] = i;
		j[i] = i;
		x[i] = i;
		y[i] = i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  i;
		g[i] =  i;
		j[i] =  i;
		x[i] =  i;
		y[i] =  i;
		b[i] =  i;
		c[i] =  i;
		d[i] =  i;
		e[i] =  i;
		z[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  25;
		g[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  25;
		g[i] =  25;
		j[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = 25;
		g[i] = 25;
		j[i] = 25;
		x[i] = 25;
		y[i] = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  25;
		g[i] =  25;
		j[i] =  25;
		x[i] =  25;
		y[i] =  25;
		b[i] =  25;
		c[i] =  25;
		d[i] =  25;
		e[i] =  25;
		z[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}

}

void mem_elops_loc_arr_ASSIGN_l2() {

	int i = 0, k = 0, b[30][30], c[30][30], d[30][30], e[30][30], f[30][30], g[30][30], h[30][30], j[30][30], x[30][30], y[30][30], z[30][30];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] =  g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}

	

	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
			j[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
			j[i][k] = f[i][k] ;
			x[i][k] = f[i][k] ;
			y[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	};



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
			j[i][k] = f[i][k] ;
			x[i][k] = f[i][k] ;
			y[i][k] = f[i][k] ;
			b[i][k] = f[i][k] ;
			c[i][k] = f[i][k] ;
			d[i][k] = f[i][k] ;
			e[i][k] = f[i][k] ;
			z[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
			j[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
			j[i][k] =  i;
			x[i][k] =  i;
			y[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
			j[i][k] =  i;
			x[i][k] =  i;
			y[i][k] =  i;
			b[i][k] =  i;
			c[i][k] =  i;
			d[i][k] =  i;
			e[i][k] =  i;
			z[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = 25;
			g[i][k] = 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  25;
			g[i][k] =  25;
			j[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = 25;
			g[i][k] = 25;
			j[i][k] = 25;
			x[i][k] = 25;
			y[i][k] = 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  25;
			g[i][k] =  25;
			j[i][k] =  25;
			x[i][k] =  25;
			y[i][k] =  25;
			b[i][k] =  25;
			c[i][k] =  25;
			d[i][k] =  25;
			e[i][k] =  25;
			z[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}

}


void mem_elops_loc_arr_ASSIGN_l3() {

	int i = 0, k = 0, n, b[10][10][10], c[10][10][10], d[10][10][10], e[10][10][10], f[10][10][10], g[10][10][10], h[10][10][10], j[10][10][10], x[10][10][10], y[10][10][10], z[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] =  g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n];
				g[i][k][n] = f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] ;
				g[i][k][n] = f[i][k][n] ;
				j[i][k][n] = f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n];
				g[i][k][n] = f[i][k][n];
				j[i][k][n] = f[i][k][n];
				x[i][k][n] = f[i][k][n];
				y[i][k][n] = f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	};



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] ;
				g[i][k][n] = f[i][k][n] ;
				j[i][k][n] = f[i][k][n] ;
				x[i][k][n] = f[i][k][n] ;
				y[i][k][n] = f[i][k][n] ;
				b[i][k][n] = f[i][k][n] ;
				c[i][k][n] = f[i][k][n] ;
				d[i][k][n] = f[i][k][n] ;
				e[i][k][n] = f[i][k][n] ;
				z[i][k][n] = f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  i;
				g[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  i;
				g[i][k][n] =  i;
				j[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = i;
				g[i][k][n] = i;
				j[i][k][n] = i;
				x[i][k][n] = i;
				y[i][k][n] = i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  i;
				g[i][k][n] =  i;
				j[i][k][n] =  i;
				x[i][k][n] =  i;
				y[i][k][n] =  i;
				b[i][k][n] =  i;
				c[i][k][n] =  i;
				d[i][k][n] =  i;
				e[i][k][n] =  i;
				z[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  25;
				g[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = 25;
				g[i][k][n] = 25;
				j[i][k][n] = 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = 25;
				g[i][k][n] = 25;
				j[i][k][n] = 25;
				x[i][k][n] = 25;
				y[i][k][n] = 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  25;
				g[i][k][n] =  25;
				j[i][k][n] =  25;
				x[i][k][n] =  25;
				y[i][k][n] =  25;
				b[i][k][n] =  25;
				c[i][k][n] =  25;
				d[i][k][n] =  25;
				e[i][k][n] =  25;
				z[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}

}

void mem_elops_loc_arr_ASSIGN_cmplx() {


	int i = 0, f[1000], g[2000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_loc_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = g[i + 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "MEM_loc_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10 + i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10 + i + 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_loc_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10 + i * 5] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}
}

void mem_elops_loc_arr_BLOCK() {

	int f[1000], g[1000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_loc_arr - BLOCK");

	t_start = get_timer_value();

	memcpy(g, f, 1000 * sizeof(int));

	t_end = get_timer_value();
	duration = (t_end - t_start) ;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);
}

void mem_elops_loc_var_PROC() {
	int i, a;
	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_loc_var - PROC");

	t_start = get_timer_value();

	for (i = 0; i < 1000; i++) {
		a = rvdummy_func(i);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);
}

void mem_elops_loc_arr_PROC() {

	int i;
	int f[1000], g[1000];
	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_loc_arr - PROC");

	t_start = get_timer_value();

	for (i = 0; i < 1000; i++) {
		g[i] = rdummy_func(f);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);
}

int rvdummy_func(int var) {
	return var;
}

int rdummy_func(int arr[]) {
	return arr[0];
}