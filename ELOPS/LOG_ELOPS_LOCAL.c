#include "LOG_ELOPS.h"

void log_elops_loc_var_LOG() {

	int a = 0, b = 20, c = -78, d = 45, e = -24, f = 89, g = 56, h = 98, j = -24, x = 7819, y = 567;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "LOG_loc_var - LOG");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c & a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;




	strcpy(op_name, "LOG_loc_var - LOG|seq_op=2");

	t_start = get_timer_value();

	b = 15;
	for (a = 0; a<1000; a++) {
		c = a & b & c;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;



	strcpy(op_name, "LOG_loc_var - LOG|seq_op=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a & c & b & d;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;



	strcpy(op_name, "LOG_loc_var - LOG|seq_op=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a & b & c & e & d & f;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;



	strcpy(op_name, "LOG_loc_var - LOG|seq_op=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a & b & c & e & d & f & g & h & j & x & y;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;




	strcpy(op_name, "LOG_loc_var - LOG|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a & c;
		b = a & b;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c;



	strcpy(op_name, "LOG_loc_var - LOG|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a & c;
		b = a & b;
		d = a & d;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c & d;



	strcpy(op_name, "LOG_loc_var - LOG|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a & c;
		b = a & b;
		d = a & d;
		e = a & e;
		f = a & f;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c & d & e & f;



	strcpy(op_name, "LOG_loc_var - LOG|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a & c;
		b = a & b;
		d = a & d;
		e = a & e;
		f = a & f;
		g = a & g;
		h = a & h;
		j = a & j;
		x = a & x;
		y = a & y;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c & d & e & f & g & h & j & x & y;




	strcpy(op_name, "LOG_loc_var - LOG|const");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;




	strcpy(op_name, "LOG_loc_var - LOG|const|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c & 5;
		b = b & 10;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c;



	strcpy(op_name, "LOG_loc_var - LOG|const|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c & 5;
		b = b & 10;
		d = d & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c & d;



	strcpy(op_name, "LOG_loc_var - LOG|const|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c & 5;
		b = b & 10;
		d = d & 5;
		e = e & 10;
		f = f & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c & d & e & f;



	strcpy(op_name, "LOG_loc_var - LOG|const|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c & 5;
		b = b & 10;
		d = d & 5;
		e = e & 10;
		f = f & 5;
		g = g & 5;
		h = h & 10;
		j = j & 5;
		x = x & 10;
		y = y & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b & c & d & e & f & g & h & j & x & y;

}

void log_elops_loc_arr_LOG_l1() {

	int i = 0, b[1000], c[1000], d[1000], e[1000], f[1000], g[1000], h[1000], j[1000], x[1000], y[1000], z[1000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] & g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] & g[i] & h[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] & g[i] & h[i] & j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] & g[i] & h[i] & j[i] & x[i] & y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] & g[i] & h[i] & j[i] & x[i] & y[i] & b[i] & c[i] & d[i] & e[i] & z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] & h[i];
		g[i] = f[i] & g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i];
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] & h[i];
		g[i] = f[i] & g[i];
		j[i] = f[i] & j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i];
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] & h[i];
		g[i] = f[i] & g[i];
		j[i] = f[i] & j[i];
		x[i] = f[i] & x[i];
		y[i] = f[i] & y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i] & x[i] & y[i];
	};



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] & h[i];
		g[i] = f[i] & g[i];
		j[i] = f[i] & j[i];
		x[i] = f[i] & x[i];
		y[i] = f[i] & y[i];
		b[i] = f[i] & b[i];
		c[i] = f[i] & c[i];
		d[i] = f[i] & d[i];
		e[i] = f[i] & e[i];
		z[i] = f[i] & z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i] & x[i] & y[i] & b[i] & c[i] & d[i] & e[i] & z[i];
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] & i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & i;
		g[i] = g[i] & i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i];
	}


	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & i;
		g[i] = g[i] & i;
		j[i] = j[i] & i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & i;
		g[i] = g[i] & i;
		j[i] = j[i] & i;
		x[i] = x[i] & i;
		y[i] = y[i] & i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i] & x[i] & y[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & i;
		g[i] = g[i] & i;
		j[i] = j[i] & i;
		x[i] = x[i] & i;
		y[i] = y[i] & i;
		b[i] = b[i] & i;
		c[i] = c[i] & i;
		d[i] = d[i] & i;
		e[i] = e[i] & i;
		z[i] = z[i] & i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i] & x[i] & y[i] & b[i] & c[i] & d[i] & e[i] & z[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & 5;
		g[i] = g[i] & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & 5;
		g[i] = g[i] & 5;
		j[i] = j[i] & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & 5;
		g[i] = g[i] & 5;
		j[i] = j[i] & 5;
		x[i] = x[i] & 5;
		y[i] = y[i] & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i] & x[i] & y[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] & 5;
		g[i] = g[i] & 5;
		j[i] = j[i] & 5;
		x[i] = x[i] & 5;
		y[i] = y[i] & 5;
		b[i] = b[i] & 5;
		c[i] = c[i] & 5;
		d[i] = d[i] & 5;
		e[i] = e[i] & 5;
		z[i] = z[i] & 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] & h[i] & j[i] & x[i] & y[i] & b[i] & c[i] & d[i] & e[i] & z[i];
	}

}

void log_elops_loc_arr_LOG_l2() {

	int i = 0, k = 0, b[30][30], c[30][30], d[30][30], e[30][30], f[30][30], g[30][30], h[30][30], j[30][30], x[30][30], y[30][30], z[30][30];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] & g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] & g[i][k] & h[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] & g[i][k] & h[i][k] & j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] & g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] & g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k] & b[i][k] & c[i][k] & d[i][k] & e[i][k] & z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] & h[i][k];
			g[i][k] = f[i][k] & g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] & h[i][k];
			g[i][k] = f[i][k] & g[i][k];
			j[i][k] = f[i][k] & j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] & h[i][k];
			g[i][k] = f[i][k] & g[i][k];
			j[i][k] = f[i][k] & j[i][k];
			x[i][k] = f[i][k] & x[i][k];
			y[i][k] = f[i][k] & y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k];
		}
	};



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] & h[i][k];
			g[i][k] = f[i][k] & g[i][k];
			j[i][k] = f[i][k] & j[i][k];
			x[i][k] = f[i][k] & x[i][k];
			y[i][k] = f[i][k] & y[i][k];
			b[i][k] = f[i][k] & b[i][k];
			c[i][k] = f[i][k] & c[i][k];
			d[i][k] = f[i][k] & d[i][k];
			e[i][k] = f[i][k] & e[i][k];
			z[i][k] = f[i][k] & z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k] & b[i][k] & c[i][k] & d[i][k] & e[i][k] & z[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] & i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & i;
			g[i][k] = g[i][k] & i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k];
		}
	}


	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & i;
			g[i][k] = g[i][k] & i;
			j[i][k] = j[i][k] & i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & i;
			g[i][k] = g[i][k] & i;
			j[i][k] = j[i][k] & i;
			x[i][k] = x[i][k] & i;
			y[i][k] = y[i][k] & i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & i;
			g[i][k] = g[i][k] & i;
			j[i][k] = j[i][k] & i;
			x[i][k] = x[i][k] & i;
			y[i][k] = y[i][k] & i;
			b[i][k] = b[i][k] & i;
			c[i][k] = c[i][k] & i;
			d[i][k] = d[i][k] & i;
			e[i][k] = e[i][k] & i;
			z[i][k] = z[i][k] & i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k] & b[i][k] & c[i][k] & d[i][k] & e[i][k] & z[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] & 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & 5;
			g[i][k] = g[i][k] & 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & 5;
			g[i][k] = g[i][k] & 5;
			j[i][k] = j[i][k] & 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & 5;
			g[i][k] = g[i][k] & 5;
			j[i][k] = j[i][k] & 5;
			x[i][k] = x[i][k] & 5;
			y[i][k] = y[i][k] & 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] & 5;
			g[i][k] = g[i][k] & 5;
			j[i][k] = j[i][k] & 5;
			x[i][k] = x[i][k] & 5;
			y[i][k] = y[i][k] & 5;
			b[i][k] = b[i][k] & 5;
			c[i][k] = c[i][k] & 5;
			d[i][k] = d[i][k] & 5;
			e[i][k] = e[i][k] & 5;
			z[i][k] = z[i][k] & 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] & h[i][k] & j[i][k] & x[i][k] & y[i][k] & b[i][k] & c[i][k] & d[i][k] & e[i][k] & z[i][k];
		}
	}

}


void log_elops_loc_arr_LOG_l3() {

	int i = 0, k = 0, n, b[10][10][10], c[10][10][10], d[10][10][10], e[10][10][10], f[10][10][10], g[10][10][10], h[10][10][10], j[10][10][10], x[10][10][10], y[10][10][10], z[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] & g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] & g[i][k][n] & h[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] & g[i][k][n] & h[i][k][n] & j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] & g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] & g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n] & b[i][k][n] & c[i][k][n] & d[i][k][n] & e[i][k][n] & z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] & h[i][k][n];
				g[i][k][n] = f[i][k][n] & g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] & h[i][k][n];
				g[i][k][n] = f[i][k][n] & g[i][k][n];
				j[i][k][n] = f[i][k][n] & j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] & h[i][k][n];
				g[i][k][n] = f[i][k][n] & g[i][k][n];
				j[i][k][n] = f[i][k][n] & j[i][k][n];
				x[i][k][n] = f[i][k][n] & x[i][k][n];
				y[i][k][n] = f[i][k][n] & y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n];
			}
		}
	};



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] & h[i][k][n];
				g[i][k][n] = f[i][k][n] & g[i][k][n];
				j[i][k][n] = f[i][k][n] & j[i][k][n];
				x[i][k][n] = f[i][k][n] & x[i][k][n];
				y[i][k][n] = f[i][k][n] & y[i][k][n];
				b[i][k][n] = f[i][k][n] & b[i][k][n];
				c[i][k][n] = f[i][k][n] & c[i][k][n];
				d[i][k][n] = f[i][k][n] & d[i][k][n];
				e[i][k][n] = f[i][k][n] & e[i][k][n];
				z[i][k][n] = f[i][k][n] & z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n] & b[i][k][n] & c[i][k][n] & d[i][k][n] & e[i][k][n] & z[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] & i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & i;
				g[i][k][n] = g[i][k][n] & i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n];
			}
		}
	}


	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & i;
				g[i][k][n] = g[i][k][n] & i;
				j[i][k][n] = j[i][k][n] & i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & i;
				g[i][k][n] = g[i][k][n] & i;
				j[i][k][n] = j[i][k][n] & i;
				x[i][k][n] = x[i][k][n] & i;
				y[i][k][n] = y[i][k][n] & i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & i;
				g[i][k][n] = g[i][k][n] & i;
				j[i][k][n] = j[i][k][n] & i;
				x[i][k][n] = x[i][k][n] & i;
				y[i][k][n] = y[i][k][n] & i;
				b[i][k][n] = b[i][k][n] & i;
				c[i][k][n] = c[i][k][n] & i;
				d[i][k][n] = d[i][k][n] & i;
				e[i][k][n] = e[i][k][n] & i;
				z[i][k][n] = z[i][k][n] & i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n] & b[i][k][n] & c[i][k][n] & d[i][k][n] & e[i][k][n] & z[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] & 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & 5;
				g[i][k][n] = g[i][k][n] & 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & 5;
				g[i][k][n] = g[i][k][n] & 5;
				j[i][k][n] = j[i][k][n] & 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & 5;
				g[i][k][n] = g[i][k][n] & 5;
				j[i][k][n] = j[i][k][n] & 5;
				x[i][k][n] = x[i][k][n] & 5;
				y[i][k][n] = y[i][k][n] & 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - LOG|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] & 5;
				g[i][k][n] = g[i][k][n] & 5;
				j[i][k][n] = j[i][k][n] & 5;
				x[i][k][n] = x[i][k][n] & 5;
				y[i][k][n] = y[i][k][n] & 5;
				b[i][k][n] = b[i][k][n] & 5;
				c[i][k][n] = c[i][k][n] & 5;
				d[i][k][n] = d[i][k][n] & 5;
				e[i][k][n] = e[i][k][n] & 5;
				z[i][k][n] = z[i][k][n] & 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] & h[i][k][n] & j[i][k][n] & x[i][k][n] & y[i][k][n] & b[i][k][n] & c[i][k][n] & d[i][k][n] & e[i][k][n] & z[i][k][n];
			}
		}
	}

}

void log_elops_loc_arr_LOG_cmplx() {


	int i = 0, f[10000], g[1000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "LOG_loc_arr - LOG|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i + 10] & g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10] & g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "LOG_loc_arr - LOG|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i] & g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i + 10] & g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - LOG|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i * 5] & g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}
}


void log_elops_loc_var_SHIFT() {

	int a = 0, b = 20, c = -78, d = 45, e = -24, f = 89, g = 56, h = 98, j = -24, x = 7819, y = 567;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "LOG_loc_var - SHIFT");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c << a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;




	strcpy(op_name, "LOG_loc_var - SHIFT|seq_op=2");

	t_start = get_timer_value();

	b = 15;
	for (a = 0; a<1000; a++) {
		c = a << b << c;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;



	strcpy(op_name, "LOG_loc_var - SHIFT|seq_op=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a << c << b << d;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;



	strcpy(op_name, "LOG_loc_var - SHIFT|seq_op=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a << b << c << e << d << f;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;



	strcpy(op_name, "LOG_loc_var - SHIFT|seq_op=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a << b << c << e << d << f << g << h << j << x << y;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;




	strcpy(op_name, "LOG_loc_var - SHIFT|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a << c;
		b = a << b;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c;



	strcpy(op_name, "LOG_loc_var - SHIFT|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a << c;
		b = a << b;
		d = a << d;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d;



	strcpy(op_name, "LOG_loc_var - SHIFT|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a << c;
		b = a << b;
		d = a << d;
		e = a << e;
		f = a << f;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f;



	strcpy(op_name, "LOG_loc_var - SHIFT|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = a << c;
		b = a << b;
		d = a << d;
		e = a << e;
		f = a << f;
		g = a << g;
		h = a << h;
		j = a << j;
		x = a << x;
		y = a << y;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f + g + h + j + x + y;




	strcpy(op_name, "LOG_loc_var - SHIFT|const");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = c;




	strcpy(op_name, "LOG_loc_var - SHIFT|const|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c << 5;
		b = b << 10;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c;



	strcpy(op_name, "LOG_loc_var - SHIFT|const|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c << 5;
		b = b << 10;
		d = d << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d;



	strcpy(op_name, "LOG_loc_var - SHIFT|const|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c << 5;
		b = b << 10;
		d = d << 5;
		e = e << 10;
		f = f << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f;



	strcpy(op_name, "LOG_loc_var - SHIFT|const|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		c = c << 5;
		b = b << 10;
		d = d << 5;
		e = e << 10;
		f = f << 5;
		g = g << 5;
		h = h << 10;
		j = j << 5;
		x = x << 10;
		y = y << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = b + c + d + e + f + g + h + j + x + y;

}

void log_elops_loc_arr_SHIFT_l1() {

	int i = 0, b[1000], c[1000], d[1000], e[1000], f[1000], g[1000], h[1000], j[1000], x[1000], y[1000], z[1000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] << g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] << g[i] << h[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] << g[i] << h[i] << j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] << g[i] << h[i] << j[i] << x[i] << y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] << g[i] << h[i] << j[i] << x[i] << y[i] << b[i] << c[i] << d[i] << e[i] << z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] << h[i];
		g[i] = f[i] << g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] << h[i];
		g[i] = f[i] << g[i];
		j[i] = f[i] << j[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] << h[i];
		g[i] = f[i] << g[i];
		j[i] = f[i] << j[i];
		x[i] = f[i] << x[i];
		y[i] = f[i] << y[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	};



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] << h[i];
		g[i] = f[i] << g[i];
		j[i] = f[i] << j[i];
		x[i] = f[i] << x[i];
		y[i] = f[i] << y[i];
		b[i] = f[i] << b[i];
		c[i] = f[i] << c[i];
		d[i] = f[i] << d[i];
		e[i] = f[i] << e[i];
		z[i] = f[i] << z[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] << i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << i;
		g[i] = g[i] << i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << i;
		g[i] = g[i] << i;
		j[i] = j[i] << i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << i;
		g[i] = g[i] << i;
		j[i] = j[i] << i;
		x[i] = x[i] << i;
		y[i] = y[i] << i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << i;
		g[i] = g[i] << i;
		j[i] = j[i] << i;
		x[i] = x[i] << i;
		y[i] = y[i] << i;
		b[i] = b[i] << i;
		c[i] = c[i] << i;
		d[i] = d[i] << i;
		e[i] = e[i] << i;
		z[i] = z[i] << i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i] << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << 5;
		g[i] = g[i] << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << 5;
		g[i] = g[i] << 5;
		j[i] = j[i] << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << 5;
		g[i] = g[i] << 5;
		j[i] = j[i] << 5;
		x[i] = x[i] << 5;
		y[i] = y[i] << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = h[i] << 5;
		g[i] = g[i] << 5;
		j[i] = j[i] << 5;
		x[i] = x[i] << 5;
		y[i] = y[i] << 5;
		b[i] = b[i] << 5;
		c[i] = c[i] << 5;
		d[i] = d[i] << 5;
		e[i] = e[i] << 5;
		z[i] = z[i] << 5;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}

}
					  
void log_elops_loc_arr_SHIFT_l2() {

	int i = 0, k = 0, b[30][30], c[30][30], d[30][30], e[30][30], f[30][30], g[30][30], h[30][30], j[30][30], x[30][30], y[30][30], z[30][30];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] << g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] << g[i][k] << h[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] << g[i][k] << h[i][k] << j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] << g[i][k] << h[i][k] << j[i][k] << x[i][k] << y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] << g[i][k] << h[i][k] << j[i][k] << x[i][k] << y[i][k] << b[i][k] << c[i][k] << d[i][k] << e[i][k] << z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] << h[i][k];
			g[i][k] = f[i][k] << g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] << h[i][k];
			g[i][k] = f[i][k] << g[i][k];
			j[i][k] = f[i][k] << j[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] << h[i][k];
			g[i][k] = f[i][k] << g[i][k];
			j[i][k] = f[i][k] << j[i][k];
			x[i][k] = f[i][k] << x[i][k];
			y[i][k] = f[i][k] << y[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	};



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] << h[i][k];
			g[i][k] = f[i][k] << g[i][k];
			j[i][k] = f[i][k] << j[i][k];
			x[i][k] = f[i][k] << x[i][k];
			y[i][k] = f[i][k] << y[i][k];
			b[i][k] = f[i][k] << b[i][k];
			c[i][k] = f[i][k] << c[i][k];
			d[i][k] = f[i][k] << d[i][k];
			e[i][k] = f[i][k] << e[i][k];
			z[i][k] = f[i][k] << z[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] << i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << i;
			g[i][k] = g[i][k] << i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k];
		}
	}


	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << i;
			g[i][k] = g[i][k] << i;
			j[i][k] = j[i][k] << i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k] << j[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << i;
			g[i][k] = g[i][k] << i;
			j[i][k] = j[i][k] << i;
			x[i][k] = x[i][k] << i;
			y[i][k] = y[i][k] << i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k] << j[i][k] << x[i][k] << y[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << i;
			g[i][k] = g[i][k] << i;
			j[i][k] = j[i][k] << i;
			x[i][k] = x[i][k] << i;
			y[i][k] = y[i][k] << i;
			b[i][k] = b[i][k] << i;
			c[i][k] = c[i][k] << i;
			d[i][k] = d[i][k] << i;
			e[i][k] = e[i][k] << i;
			z[i][k] = z[i][k] << i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k] << j[i][k] << x[i][k] << y[i][k] << b[i][k] << c[i][k] << d[i][k] << e[i][k] << z[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = f[i][k] << 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << 5;
			g[i][k] = g[i][k] << 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << 5;
			g[i][k] = g[i][k] << 5;
			j[i][k] = j[i][k] << 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k] << j[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << 5;
			g[i][k] = g[i][k] << 5;
			j[i][k] = j[i][k] << 5;
			x[i][k] = x[i][k] << 5;
			y[i][k] = y[i][k] << 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k] << j[i][k] << x[i][k] << y[i][k];
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = h[i][k] << 5;
			g[i][k] = g[i][k] << 5;
			j[i][k] = j[i][k] << 5;
			x[i][k] = x[i][k] << 5;
			y[i][k] = y[i][k] << 5;
			b[i][k] = b[i][k] << 5;
			c[i][k] = c[i][k] << 5;
			d[i][k] = d[i][k] << 5;
			e[i][k] = e[i][k] << 5;
			z[i][k] = z[i][k] << 5;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] << h[i][k] << j[i][k] << x[i][k] << y[i][k] << b[i][k] << c[i][k] << d[i][k] << e[i][k] << z[i][k];
		}
	}

}
					  
void log_elops_loc_arr_SHIFT_l3() {

	int i = 0, k = 0, n, b[10][10][10], c[10][10][10], d[10][10][10], e[10][10][10], f[10][10][10], g[10][10][10], h[10][10][10], j[10][10][10], x[10][10][10], y[10][10][10], z[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] << g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_op=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] << g[i][k][n] << h[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_op=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] << g[i][k][n] << h[i][k][n] << j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_op=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] << g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_op=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] << g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n] << b[i][k][n] << c[i][k][n] << d[i][k][n] << e[i][k][n] << z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] << h[i][k][n];
				g[i][k][n] = f[i][k][n] << g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] << h[i][k][n];
				g[i][k][n] = f[i][k][n] << g[i][k][n];
				j[i][k][n] = f[i][k][n] << j[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] << h[i][k][n];
				g[i][k][n] = f[i][k][n] << g[i][k][n];
				j[i][k][n] = f[i][k][n] << j[i][k][n];
				x[i][k][n] = f[i][k][n] << x[i][k][n];
				y[i][k][n] = f[i][k][n] << y[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n];
			}
		}
	};



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] << h[i][k][n];
				g[i][k][n] = f[i][k][n] << g[i][k][n];
				j[i][k][n] = f[i][k][n] << j[i][k][n];
				x[i][k][n] = f[i][k][n] << x[i][k][n];
				y[i][k][n] = f[i][k][n] << y[i][k][n];
				b[i][k][n] = f[i][k][n] << b[i][k][n];
				c[i][k][n] = f[i][k][n] << c[i][k][n];
				d[i][k][n] = f[i][k][n] << d[i][k][n];
				e[i][k][n] = f[i][k][n] << e[i][k][n];
				z[i][k][n] = f[i][k][n] << z[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n] << b[i][k][n] << c[i][k][n] << d[i][k][n] << e[i][k][n] << z[i][k][n];
			}
		}
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] << i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << i;
				g[i][k][n] = g[i][k][n] << i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n];
			}
		}
	}


	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << i;
				g[i][k][n] = g[i][k][n] << i;
				j[i][k][n] = j[i][k][n] << i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << i;
				g[i][k][n] = g[i][k][n] << i;
				j[i][k][n] = j[i][k][n] << i;
				x[i][k][n] = x[i][k][n] << i;
				y[i][k][n] = y[i][k][n] << i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << i;
				g[i][k][n] = g[i][k][n] << i;
				j[i][k][n] = j[i][k][n] << i;
				x[i][k][n] = x[i][k][n] << i;
				y[i][k][n] = y[i][k][n] << i;
				b[i][k][n] = b[i][k][n] << i;
				c[i][k][n] = c[i][k][n] << i;
				d[i][k][n] = d[i][k][n] << i;
				e[i][k][n] = e[i][k][n] << i;
				z[i][k][n] = z[i][k][n] << i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n] << b[i][k][n] << c[i][k][n] << d[i][k][n] << e[i][k][n] << z[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = f[i][k][n] << 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << 5;
				g[i][k][n] = g[i][k][n] << 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << 5;
				g[i][k][n] = g[i][k][n] << 5;
				j[i][k][n] = j[i][k][n] << 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << 5;
				g[i][k][n] = g[i][k][n] << 5;
				j[i][k][n] = j[i][k][n] << 5;
				x[i][k][n] = x[i][k][n] << 5;
				y[i][k][n] = y[i][k][n] << 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n];
			}
		}
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = h[i][k][n] << 5;
				g[i][k][n] = g[i][k][n] << 5;
				j[i][k][n] = j[i][k][n] << 5;
				x[i][k][n] = x[i][k][n] << 5;
				y[i][k][n] = y[i][k][n] << 5;
				b[i][k][n] = b[i][k][n] << 5;
				c[i][k][n] = c[i][k][n] << 5;
				d[i][k][n] = d[i][k][n] << 5;
				e[i][k][n] = e[i][k][n] << 5;
				z[i][k][n] = z[i][k][n] << 5;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] << h[i][k][n] << j[i][k][n] << x[i][k][n] << y[i][k][n] << b[i][k][n] << c[i][k][n] << d[i][k][n] << e[i][k][n] << z[i][k][n];
			}
		}
	}

}
					   
void log_elops_loc_arr_SHIFT_cmplx() {


	int i = 0, f[10000], g[1000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "LOG_loc_arr - SHIFT|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = f[i + 10] << g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10] << g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "LOG_loc_arr - SHIFT|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i] << g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i + 10] << g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "LOG_loc_arr - SHIFT|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = f[i * 10 + i * 5] << g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}
}


