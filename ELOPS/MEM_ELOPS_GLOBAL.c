#include "MEM_ELOPS.h"



void mem_elops_glob_var_ASSIGN() {

	int a = 0;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform


	timer_setup();


	strcpy(op_name, "MEM_glob_var - ASSIGN");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_c;





	strcpy(op_name, "MEM_glob_var - ASSIGN|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = g_a ;
		g_b = g_a ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c;



	strcpy(op_name, "MEM_glob_var - ASSIGN|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = g_a ;
		g_b = g_a ;
		g_d = g_a ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d;



	strcpy(op_name, "MEM_glob_var - ASSIGN|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = g_a ;
		g_b = g_a ;
		g_d = g_a ;
		g_e = g_a ;
		g_f = g_a ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d + g_e + g_f;



	strcpy(op_name, "MEM_glob_var - ASSIGN|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = g_a ;
		g_b = g_a ;
		g_d = g_a ;
		g_e = g_a ;
		g_f = g_a ;
		g_g = g_a ;
		g_h = g_a ;
		g_j = g_a ;
		g_x = g_a ;
		g_y = g_a ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d + g_e + g_f + g_g + g_h + g_j + g_x + g_y;




	strcpy(op_name, "MEM_glob_var - ASSIGN|const");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = g_c + 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_c;




	strcpy(op_name, "MEM_glob_var - ASSIGN|const|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c =25;
		g_b =75;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c;



	strcpy(op_name, "MEM_glob_var - ASSIGN|const|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c =  25;
		g_b =  75;
		g_d =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d;



	strcpy(op_name, "MEM_glob_var - ASSIGN|const|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = 25;
		g_b = 75;
		g_d = 25;
		g_e = 75;
		g_f = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d + g_e + g_f;



	strcpy(op_name, "MEM_glob_var - ASSIGN|const|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c =25;
		g_b =75;
		g_d =25;
		g_e =75;
		g_f =25;
		g_g =25;
		g_h =75;
		g_j =25;
		g_x =75;
		g_y =25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d + g_e + g_f + g_g + g_h + g_j + g_x + g_y;



	strcpy(op_name, "MEM_glob_var - ASSIGN|loc_var");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c =  a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_c;




	strcpy(op_name, "MEM_glob_var - ASSIGN|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c = a;
		g_b = a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c;



	strcpy(op_name, "MEM_glob_var - ASSIGN|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c =  a;
		g_b =  a;
		g_d =  a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d;



	strcpy(op_name, "MEM_glob_var - ASSIGN|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c =  a;
		g_b =  a;
		g_d =  a;
		g_e =  a;
		g_f =  a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d + g_e + g_f;



	strcpy(op_name, "MEM_glob_var - ASSIGN|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (a = 0; a<1000; a++) {
		g_c =  a;
		g_b =  a;
		g_d =  a;
		g_e =  a;
		g_f =  a;
		g_g =  a;
		g_h =  a;
		g_j =  a;
		g_x =  a;
		g_y =  a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)a;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = g_b + g_c + g_d + g_e + g_f + g_g + g_h + g_j + g_x + g_y;

}

void mem_elops_glob_arr_ASSIGN_l1() {


	int i = 0, g[1000];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_f[i] =  gf_g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}


	

	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] = gf_f[i] ;
		gf_g[i] = gf_f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i];
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] = gf_f[i];
		gf_g[i] = gf_f[i];
		gf_j[i] = gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i];
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] = gf_f[i] ;
		gf_g[i] = gf_f[i] ;
		gf_j[i] = gf_f[i] ;
		gf_x[i] = gf_f[i] ;
		gf_y[i] = gf_f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i];
	};



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] = gf_f[i] ;
		gf_g[i] = gf_f[i] ;
		gf_j[i] = gf_f[i] ;
		gf_x[i] = gf_f[i] ;
		gf_y[i] = gf_f[i] ;
		gf_b[i] = gf_f[i] ;
		gf_c[i] = gf_f[i] ;
		gf_d[i] = gf_f[i] ;
		gf_e[i] = gf_f[i] ;
		gf_z[i] = gf_f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i] + gf_b[i] + gf_c[i] + gf_d[i] + gf_e[i] + gf_z[i];

	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_f[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  25;
		gf_g[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  25;
		gf_g[i] =  25;
		gf_j[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  25;
		gf_g[i] =  25;
		gf_j[i] =  25;
		gf_x[i] =  25;
		gf_y[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  25;
		gf_g[i] =  25;
		gf_j[i] =  25;
		gf_x[i] =  25;
		gf_y[i] =  25;
		gf_b[i] =  25;
		gf_c[i] =  25;
		gf_d[i] =  25;
		gf_e[i] =  25;
		gf_z[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i] + gf_b[i] + gf_c[i] + gf_d[i] + gf_e[i] + gf_z[i];
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_f[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  g_a;
		gf_g[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i];
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  g_a;
		gf_g[i] =  g_a;
		gf_j[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  g_a;
		gf_g[i] =  g_a;
		gf_j[i] =  g_a;
		gf_x[i] =  g_a;
		gf_y[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  g_a;
		gf_g[i] =  g_a;
		gf_j[i] =  g_a;
		gf_x[i] =  g_a;
		gf_y[i] =  g_a;
		gf_b[i] =  g_a;
		gf_c[i] =  g_a;
		gf_d[i] =  g_a;
		gf_e[i] =  g_a;
		gf_z[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i] + gf_b[i] + gf_c[i] + gf_d[i] + gf_e[i] + gf_z[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_f[i] = i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  i;
		gf_g[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i];
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  i;
		gf_g[i] =  i;
		gf_j[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  i;
		gf_g[i] =  i;
		gf_j[i] =  i;
		gf_x[i] =  i;
		gf_y[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  i;
		gf_g[i] =  i;
		gf_j[i] =  i;
		gf_x[i] =  i;
		gf_y[i] =  i;
		gf_b[i] =  i;
		gf_c[i] =  i;
		gf_d[i] =  i;
		gf_e[i] =  i;
		gf_z[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i] + gf_b[i] + gf_c[i] + gf_d[i] + gf_e[i] + gf_z[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_f[i] = g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  g[i];
		gf_g[i] =  g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i];
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  g[i];
		gf_g[i] =  g[i];
		gf_j[i] =  g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i];
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] = g[i];
		gf_g[i] = g[i];
		gf_j[i] = g[i];
		gf_x[i] = g[i];
		gf_y[i] = g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i];
	};



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_h[i] =  g[i];
		gf_g[i] =  g[i];
		gf_j[i] =  g[i];
		gf_x[i] =  g[i];
		gf_y[i] =  g[i];
		gf_b[i] =  g[i];
		gf_c[i] =  g[i];
		gf_d[i] =  g[i];
		gf_e[i] =  g[i];
		gf_z[i] =  g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_g[i] + gf_h[i] + gf_j[i] + gf_x[i] + gf_y[i] + gf_b[i] + gf_c[i] + gf_d[i] + gf_e[i] + gf_z[i];

	}

}


void mem_elops_glob_arr_ASSIGN_l2() {

	int g[30][30], i = 0, k = 0;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_f[i][k] = l2_gf_g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_f[i][k];
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = l2_gf_f[i][k];
			l2_gf_g[i][k] = l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k];
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = l2_gf_f[i][k] ;
			l2_gf_g[i][k] = l2_gf_f[i][k] ;
			l2_gf_j[i][k] = l2_gf_f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k];
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = l2_gf_f[i][k] ;
			l2_gf_g[i][k] = l2_gf_f[i][k] ;
			l2_gf_j[i][k] = l2_gf_f[i][k] ;
			l2_gf_x[i][k] = l2_gf_f[i][k] ;
			l2_gf_y[i][k] = l2_gf_f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k];
		}
	};



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = l2_gf_f[i][k];
			l2_gf_g[i][k] = l2_gf_f[i][k];
			l2_gf_j[i][k] = l2_gf_f[i][k];
			l2_gf_x[i][k] = l2_gf_f[i][k];
			l2_gf_y[i][k] = l2_gf_f[i][k];
			l2_gf_b[i][k] = l2_gf_f[i][k];
			l2_gf_c[i][k] = l2_gf_f[i][k];
			l2_gf_d[i][k] = l2_gf_f[i][k];
			l2_gf_e[i][k] = l2_gf_f[i][k];
			l2_gf_z[i][k] = l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k] + l2_gf_b[i][k] + l2_gf_c[i][k] + l2_gf_d[i][k] + l2_gf_e[i][k] + l2_gf_z[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_f[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_f[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = 25;
			l2_gf_g[i][k] = 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = 25;
			l2_gf_g[i][k] = 25;
			l2_gf_j[i][k] = 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  25;
			l2_gf_g[i][k] =  25;
			l2_gf_j[i][k] =  25;
			l2_gf_x[i][k] =  25;
			l2_gf_y[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  25;
			l2_gf_g[i][k] =  25;
			l2_gf_j[i][k] =  25;
			l2_gf_x[i][k] =  25;
			l2_gf_y[i][k] =  25;
			l2_gf_b[i][k] =  25;
			l2_gf_c[i][k] =  25;
			l2_gf_d[i][k] =  25;
			l2_gf_e[i][k] =  25;
			l2_gf_z[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k] + l2_gf_b[i][k] + l2_gf_c[i][k] + l2_gf_d[i][k] + l2_gf_e[i][k] + l2_gf_z[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_f[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_f[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  g_a;
			l2_gf_g[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k];
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  g_a;
			l2_gf_g[i][k] =  g_a;
			l2_gf_j[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  g_a;
			l2_gf_g[i][k] =  g_a;
			l2_gf_j[i][k] =  g_a;
			l2_gf_x[i][k] =  g_a;
			l2_gf_y[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  g_a;
			l2_gf_g[i][k] =  g_a;
			l2_gf_j[i][k] =  g_a;
			l2_gf_x[i][k] =  g_a;
			l2_gf_y[i][k] =  g_a;
			l2_gf_b[i][k] =  g_a;
			l2_gf_c[i][k] =  g_a;
			l2_gf_d[i][k] =  g_a;
			l2_gf_e[i][k] =  g_a;
			l2_gf_z[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k] + l2_gf_b[i][k] + l2_gf_c[i][k] + l2_gf_d[i][k] + l2_gf_e[i][k] + l2_gf_z[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_f[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_f[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  i;
			l2_gf_g[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k];
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  i;
			l2_gf_g[i][k] =  i;
			l2_gf_j[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  i;
			l2_gf_g[i][k] =  i;
			l2_gf_j[i][k] =  i;
			l2_gf_x[i][k] =  i;
			l2_gf_y[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k];
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] =  i;
			l2_gf_g[i][k] =  i;
			l2_gf_j[i][k] =  i;
			l2_gf_x[i][k] =  i;
			l2_gf_y[i][k] =  i;
			l2_gf_b[i][k] =  i;
			l2_gf_c[i][k] =  i;
			l2_gf_d[i][k] =  i;
			l2_gf_e[i][k] =  i;
			l2_gf_z[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k] + l2_gf_b[i][k] + l2_gf_c[i][k] + l2_gf_d[i][k] + l2_gf_e[i][k] + l2_gf_z[i][k];
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_f[i][k] = g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_f[i][k];
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = g[i][k] ;
			l2_gf_g[i][k] = g[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k];
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = g[i][k] ;
			l2_gf_g[i][k] = g[i][k] ;
			l2_gf_j[i][k] = g[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k];
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = g[i][k] ;
			l2_gf_g[i][k] = g[i][k] ;
			l2_gf_j[i][k] = g[i][k] ;
			l2_gf_x[i][k] = g[i][k] ;
			l2_gf_y[i][k] = g[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k];
		}
	};



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			l2_gf_h[i][k] = g[i][k] ;
			l2_gf_g[i][k] = g[i][k] ;
			l2_gf_j[i][k] = g[i][k] ;
			l2_gf_x[i][k] = g[i][k] ;
			l2_gf_y[i][k] = g[i][k] ;
			l2_gf_b[i][k] = g[i][k] ;
			l2_gf_c[i][k] = g[i][k] ;
			l2_gf_d[i][k] = g[i][k] ;
			l2_gf_e[i][k] = g[i][k] ;
			l2_gf_z[i][k] = g[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = l2_gf_g[i][k] + l2_gf_h[i][k] + l2_gf_j[i][k] + l2_gf_x[i][k] + l2_gf_y[i][k] + l2_gf_b[i][k] + l2_gf_c[i][k] + l2_gf_d[i][k] + l2_gf_e[i][k] + l2_gf_z[i][k];
		}
	}

}


void mem_elops_glob_arr_ASSIGN_l3() {

	int i = 0, k = 0, n = 0, g[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_f[i][k][n] = l3_gf_g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_f[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_g[i][k][n] = l3_gf_f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_g[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_j[i][k][n] = l3_gf_f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n]  +l3_gf_h[i][k][n]+ l3_gf_j[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_g[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_j[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_x[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_y[i][k][n] = l3_gf_f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n] + l3_gf_x[i][k][n]+l3_gf_y[i][k][n];
			}
		}
	};



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_g[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_j[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_x[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_y[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_b[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_c[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_d[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_e[i][k][n] = l3_gf_f[i][k][n] ;
				l3_gf_z[i][k][n] = l3_gf_f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n] + l3_gf_x[i][k][n] + l3_gf_y[i][k][n]  +   l3_gf_b[i][k][n] + l3_gf_c[i][k][n] + l3_gf_d[i][k][n] + l3_gf_e[i][k][n] + l3_gf_z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_f[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  25;
				l3_gf_g[i][k][n] = 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  25;
				l3_gf_g[i][k][n] = 25;
				l3_gf_j[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  25;
				l3_gf_g[i][k][n] = 25;
				l3_gf_j[i][k][n] =  25;
				l3_gf_x[i][k][n] =  25;
				l3_gf_y[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n]   + +l3_gf_h[i][k][n] +l3_gf_j[i][k][n] + l3_gf_x[i][k][n]+ l3_gf_y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  25;
				l3_gf_g[i][k][n] = 25;
				l3_gf_j[i][k][n] =  25;
				l3_gf_x[i][k][n] =  25;
				l3_gf_y[i][k][n] =  25;
				l3_gf_b[i][k][n] = 25;
				l3_gf_c[i][k][n] = 25;
				l3_gf_d[i][k][n] = 25;
				l3_gf_e[i][k][n] = 25;
				l3_gf_z[i][k][n] = 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + +l3_gf_h[i][k][n] + l3_gf_j[i][k][n] + l3_gf_x[i][k][n] + l3_gf_y[i][k][n] +  l3_gf_b[i][k][n] + l3_gf_c[i][k][n] + l3_gf_d[i][k][n] + l3_gf_e[i][k][n] + l3_gf_z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_f[i][k][n] =  g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  g_a;
				l3_gf_g[i][k][n] = g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  g_a;
				l3_gf_g[i][k][n] = g_a;
				l3_gf_j[i][k][n] =  g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n]+ l3_gf_j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  g_a;
				l3_gf_g[i][k][n] = g_a;
				l3_gf_j[i][k][n] =  g_a;
				l3_gf_x[i][k][n] =  g_a;
				l3_gf_y[i][k][n] =  g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n]   +l3_gf_h[i][k][n] + l3_gf_j[i][k][n] + l3_gf_x[i][k][n] +l3_gf_y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  g_a;
				l3_gf_g[i][k][n] = g_a;
				l3_gf_j[i][k][n] =  g_a;
				l3_gf_x[i][k][n] =  g_a;
				l3_gf_y[i][k][n] =  g_a;
				l3_gf_b[i][k][n] =  g_a;
				l3_gf_c[i][k][n] =  g_a;
				l3_gf_d[i][k][n] =  g_a;
				l3_gf_e[i][k][n] =  g_a;
				l3_gf_z[i][k][n] =  g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n] + l3_gf_x[i][k][n] + l3_gf_y[i][k][n] +   l3_gf_b[i][k][n] + l3_gf_c[i][k][n] + l3_gf_d[i][k][n] + l3_gf_e[i][k][n] + l3_gf_z[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_f[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  i;
				l3_gf_g[i][k][n] = i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  i;
				l3_gf_g[i][k][n] = i;
				l3_gf_j[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n]  + l3_gf_h[i][k][n]+ l3_gf_j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  i;
				l3_gf_g[i][k][n] = i;
				l3_gf_j[i][k][n] =  i;
				l3_gf_x[i][k][n] =  i;
				l3_gf_y[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n]+l3_gf_x[i][k][n]   +  l3_gf_y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] =  i;
				l3_gf_g[i][k][n] = i;
				l3_gf_j[i][k][n] =  i;
				l3_gf_x[i][k][n] =  i;
				l3_gf_y[i][k][n] =  i;
				l3_gf_b[i][k][n] =  i;
				l3_gf_c[i][k][n] =  i;
				l3_gf_d[i][k][n] =  i;
				l3_gf_e[i][k][n] =  i;
				l3_gf_z[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n] + l3_gf_x[i][k][n] + l3_gf_y[i][k][n] +   l3_gf_b[i][k][n] + l3_gf_c[i][k][n] + l3_gf_d[i][k][n] + l3_gf_e[i][k][n] + l3_gf_z[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_f[i][k][n] =  g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = g[i][k][n] ;
				l3_gf_g[i][k][n] = g[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = g[i][k][n] ;
				l3_gf_g[i][k][n] = g[i][k][n] ;
				l3_gf_j[i][k][n] = g[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n]+ l3_gf_j[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = g[i][k][n] ;
				l3_gf_g[i][k][n] = g[i][k][n] ;
				l3_gf_j[i][k][n] = g[i][k][n] ;
				l3_gf_x[i][k][n] = g[i][k][n] ;
				l3_gf_y[i][k][n] = g[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n]+ l3_gf_x[i][k][n]   +  l3_gf_y[i][k][n];
			}
		}
	};



	strcpy(op_name, "MEM_glob_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				l3_gf_h[i][k][n] = g[i][k][n] ;
				l3_gf_g[i][k][n] = g[i][k][n] ;
				l3_gf_j[i][k][n] = g[i][k][n] ;
				l3_gf_x[i][k][n] = g[i][k][n];
				l3_gf_y[i][k][n] = g[i][k][n];
				l3_gf_b[i][k][n] = g[i][k][n];
				l3_gf_c[i][k][n] = g[i][k][n];
				l3_gf_d[i][k][n] = g[i][k][n];
				l3_gf_e[i][k][n] = g[i][k][n];
				l3_gf_z[i][k][n] = g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = l3_gf_g[i][k][n] + l3_gf_h[i][k][n] + l3_gf_j[i][k][n] + l3_gf_x[i][k][n] + l3_gf_y[i][k][n] +   l3_gf_b[i][k][n] + l3_gf_c[i][k][n] + l3_gf_d[i][k][n] + l3_gf_e[i][k][n] + l3_gf_z[i][k][n];
			}
		}
	}

}



void mem_elops_glob_arr_ASSIGN_cmplx() {


	int i = 0;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_glob_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		gf_f[i] = gf_g[i + 10];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		gf_f[i] = gf_g[i * 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}




	strcpy(op_name, "MEM_glob_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		gf_f[i] = gf_g[i * 10 + i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		gf_f[i] = gf_g[i * 10 + i + 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}



	strcpy(op_name, "MEM_glob_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		gf_f[i] = gf_g[i * 10 + i * 5] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = gf_f[i];
	}
}


void mem_elops_glob_arr_BLOCK() {

	

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_glob_arr - BLOCK");

	t_start = get_timer_value();

	memcpy(gf_b, gf_b, 1000 * sizeof(int));

	t_end = get_timer_value();
	duration = (t_end - t_start);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);
}

