#include "MEM_ELOPS.h"

void mem_elops_par_var_ASSIGN(int *a, int *b, int *c, int *d, int *e, int *f, int *g, int *h, int *j, int *x, int *y) {

	int i = 0;


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "MEM_par_var - ASSIGN");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);



	strcpy(op_name, "MEM_par_var - ASSIGN|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) ;
		(*b) = (*a) ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "MEM_par_var - ASSIGN|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) ;
		(*b) = (*a) ;
		(*d) = (*a) ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "MEM_par_var - ASSIGN|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a) ;
		(*b) = (*a) ;
		(*d) = (*a) ;
		(*e) = (*a) ;
		(*f) = (*a) ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "MEM_par_var - ASSIGN|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = (*a);
		(*b) = (*a);
		(*d) = (*a);
		(*e) = (*a);
		(*f) = (*a);
		(*g) = (*a);
		(*h) = (*a);
		(*j) = (*a);
		(*x) = (*a);
		(*y) = (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);




	strcpy(op_name, "MEM_par_var - ASSIGN|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "MEM_par_var - ASSIGN|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = 25;
		(*b) = 75;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "MEM_par_var - ASSIGN|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  25;
		(*b) =  75;
		(*d) =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "MEM_par_var - ASSIGN|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  25;
		(*b) =  75;
		(*d) =  25;
		(*e) =  75;
		(*f) =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "MEM_par_var - ASSIGN|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  25;
		(*b) =  75;
		(*d) =  25;
		(*e) =  75;
		(*f) =  25;
		(*g) =  25;
		(*h) =  75;
		(*j) =  25;
		(*x) =  75;
		(*y) =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);



	strcpy(op_name, "MEM_par_var - ASSIGN|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "MEM_par_var - ASSIGN|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  i;
		(*b) =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "MEM_par_var - ASSIGN|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  i;
		(*b) =  i;
		(*d) =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "MEM_par_var - ASSIGN|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) = i;
		(*b) = i;
		(*d) = i;
		(*e) = i;
		(*f) = i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "MEM_par_var - ASSIGN|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  i;
		(*b) =  i;
		(*d) =  i;
		(*e) =  i;
		(*f) =  i;
		(*g) =  i;
		(*h) =  i;
		(*j) =  i;
		(*x) =  i;
		(*y) =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);



	strcpy(op_name, "MEM_par_var - ASSIGN|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*c);




	strcpy(op_name, "MEM_par_var - ASSIGN|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  g_a;
		(*b) =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c);



	strcpy(op_name, "MEM_par_var - ASSIGN|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  g_a;
		(*b) =  g_a;
		(*d) =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d);



	strcpy(op_name, "MEM_par_var - ASSIGN|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  g_a;
		(*b) =  g_a;
		(*d) =  g_a;
		(*e) =  g_a;
		(*f) =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f);



	strcpy(op_name, "MEM_par_var - ASSIGN|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		(*c) =  g_a;
		(*b) =  g_a;
		(*d) =  g_a;
		(*e) =  g_a;
		(*f) =  g_a;
		(*g) =  g_a;
		(*h) =  g_a;
		(*j) =  g_a;
		(*x) =  g_a;
		(*y) =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	temp_res = (*b) + (*c) + (*d) + (*e) + (*f) + (*g) + (*h) + (*j) + (*x) + (*y);

}

void mem_elops_par_arr_ASSIGN_l1(int *a, int *b, int *c, int *d, int *e, int *f, int *g, int *h, int *j, int *x, int *y, int *z) {

	int i = 0, lf[1000];


	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] =  g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}





	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] + h[i];
		g[i] = f[i] + g[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] ;
		g[i] = f[i] ;
		j[i] = f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] ;
		g[i] = f[i] ;
		j[i] = f[i] ;
		x[i] = f[i] ;
		y[i] = f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	};



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = f[i] ;
		g[i] = f[i] ;
		j[i] = f[i] ;
		x[i] = f[i] ;
		y[i] = f[i] ;
		b[i] = f[i] ;
		c[i] = f[i] ;
		d[i] = f[i] ;
		e[i] = f[i] ;
		z[i] = f[i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|const");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] =  25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = 25;
		g[i] = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =25;
		g[i] =25;
		j[i] =25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = 25;
		g[i] = 25;
		j[i] = 25;
		x[i] = 25;
		y[i] = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = 25;
		g[i] = 25;
		j[i] = 25;
		x[i] = 25;
		y[i] = 25;
		b[i] = 25;
		c[i] = 25;
		d[i] = 25;
		e[i] = 25;
		z[i] = 25;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = (*a);
		g[i] = (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  (*a);
		g[i] =  (*a);
		j[i] =  (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = (*a);
		g[i] = (*a);
		j[i] = (*a);
		x[i] = (*a);
		y[i] = (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  (*a);
		g[i] =  (*a);
		j[i] =  (*a);
		x[i] =  (*a);
		y[i] =  (*a);
		b[i] =  (*a);
		c[i] =  (*a);
		d[i] =  (*a);
		e[i] =  (*a);
		z[i] =  (*a);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  i;
		g[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  i;
		g[i] =  i;
		j[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = i;
		g[i] = i;
		j[i] = i;
		x[i] = i;
		y[i] = i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  i;
		g[i] =  i;
		j[i] =  i;
		x[i] =  i;
		y[i] =  i;
		b[i] =  i;
		c[i] =  i;
		d[i] =  i;
		e[i] =  i;
		z[i] =  i;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  lf[i];
		g[i] =  lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  lf[i];
		g[i] =  lf[i];
		j[i] =  lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  lf[i];
		g[i] =  lf[i];
		j[i] =  lf[i];
		x[i] =  lf[i];
		y[i] =  lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  lf[i];
		g[i] =  lf[i];
		j[i] =  lf[i];
		x[i] =  lf[i];
		y[i] =  lf[i];
		b[i] =  lf[i];
		c[i] =  lf[i];
		d[i] =  lf[i];
		e[i] =  lf[i];
		z[i] =  lf[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  g_a;
		g[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] = g_a;
		g[i] = g_a;
		j[i] = g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  g_a;
		g[i] =  g_a;
		j[i] =  g_a;
		x[i] =  g_a;
		y[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  g_a;
		g[i] =  g_a;
		j[i] =  g_a;
		x[i] =  g_a;
		y[i] =  g_a;
		b[i] =  g_a;
		c[i] =  g_a;
		d[i] =  g_a;
		e[i] =  g_a;
		z[i] =  g_a;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] =  gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  gf_f[i];
		g[i] =  gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i];
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  gf_f[i];
		g[i] =  gf_f[i];
		j[i] =  gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  gf_f[i];
		g[i] =  gf_f[i];
		j[i] =  gf_f[i];
		x[i] =  gf_f[i];
		y[i] =  gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=1|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		h[i] =  gf_f[i];
		g[i] =  gf_f[i];
		j[i] =  gf_f[i];
		x[i] =  gf_f[i];
		y[i] =  gf_f[i];
		b[i] =  gf_f[i];
		c[i] =  gf_f[i];
		d[i] =  gf_f[i];
		e[i] =  gf_f[i];
		z[i] =  gf_f[i];
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = g[i] + h[i] + j[i] + x[i] + y[i] + b[i] + c[i] + d[i] + e[i] + z[i];
	}

}

void mem_elops_par_arr_ASSIGN_l2(int *a, int b[30][30], int c[30][30], int d[30][30], int e[30][30], int f[30][30], int g[30][30], int h[30][30], int j[30][30], int x[30][30], int y[30][30], int z[30][30]) {

	int i = 0, k = 0, lf[30][30];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] =  g[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
			j[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
			j[i][k] = f[i][k] ;
			x[i][k] = f[i][k] ;
			y[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	};



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = f[i][k] ;
			g[i][k] = f[i][k] ;
			j[i][k] = f[i][k] ;
			x[i][k] = f[i][k] ;
			y[i][k] = f[i][k] ;
			b[i][k] = f[i][k] ;
			c[i][k] = f[i][k] ;
			d[i][k] = f[i][k] ;
			e[i][k] = f[i][k] ;
			z[i][k] = f[i][k] ;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|const");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = 25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  25;
			g[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  25;
			g[i][k] =  25;
			j[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  25;
			g[i][k] =  25;
			j[i][k] =  25;
			x[i][k] =  25;
			y[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  25;
			g[i][k] =  25;
			j[i][k] =  25;
			x[i][k] =  25;
			y[i][k] =  25;
			b[i][k] =  25;
			c[i][k] =  25;
			d[i][k] =  25;
			e[i][k] =  25;
			z[i][k] =  25;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] =  (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  (*a);
			g[i][k] =  (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  (*a);
			g[i][k] =  (*a);
			j[i][k] =  (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = (*a);
			g[i][k] = (*a);
			j[i][k] = (*a);
			x[i][k] = (*a);
			y[i][k] = (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = (*a);
			g[i][k] = (*a);
			j[i][k] = (*a);
			x[i][k] = (*a);
			y[i][k] = (*a);
			b[i][k] = (*a);
			c[i][k] = (*a);
			d[i][k] = (*a);
			e[i][k] = (*a);
			z[i][k] = (*a);
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
			j[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
			j[i][k] =  i;
			x[i][k] =  i;
			y[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  i;
			g[i][k] =  i;
			j[i][k] =  i;
			x[i][k] =  i;
			y[i][k] =  i;
			b[i][k] =  i;
			c[i][k] =  i;
			d[i][k] =  i;
			e[i][k] =  i;
			z[i][k] =  i;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] =  lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = lf[i][k];
			g[i][k] = lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = lf[i][k];
			g[i][k] = lf[i][k];
			j[i][k] = lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  lf[i][k];
			g[i][k] =  lf[i][k];
			j[i][k] =  lf[i][k];
			x[i][k] =  lf[i][k];
			y[i][k] =  lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = lf[i][k];
			g[i][k] = lf[i][k];
			j[i][k] = lf[i][k];
			x[i][k] = lf[i][k];
			y[i][k] = lf[i][k];
			b[i][k] = lf[i][k];
			c[i][k] = lf[i][k];
			d[i][k] = lf[i][k];
			e[i][k] = lf[i][k];
			z[i][k] = lf[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  g_a;
			g[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  g_a;
			g[i][k] =  g_a;
			j[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  g_a;
			g[i][k] =  g_a;
			j[i][k] =  g_a;
			x[i][k] =  g_a;
			y[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  g_a;
			g[i][k] =  g_a;
			j[i][k] =  g_a;
			x[i][k] =  g_a;
			y[i][k] =  g_a;
			b[i][k] =  g_a;
			c[i][k] =  g_a;
			d[i][k] =  g_a;
			e[i][k] =  g_a;
			z[i][k] =  g_a;
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			f[i][k] = l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = f[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = l2_gf_f[i][k];
			g[i][k] = l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k];
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] = l2_gf_f[i][k];
			g[i][k] = l2_gf_f[i][k];
			j[i][k] = l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  l2_gf_f[i][k];
			g[i][k] =  l2_gf_f[i][k];
			j[i][k] =  l2_gf_f[i][k];
			x[i][k] =  l2_gf_f[i][k];
			y[i][k] =  l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k];
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=2|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			h[i][k] =  l2_gf_f[i][k];
			g[i][k] =  l2_gf_f[i][k];
			j[i][k] =  l2_gf_f[i][k];
			x[i][k] =  l2_gf_f[i][k];
			y[i][k] =  l2_gf_f[i][k];
			b[i][k] =  l2_gf_f[i][k];
			c[i][k] =  l2_gf_f[i][k];
			d[i][k] =  l2_gf_f[i][k];
			e[i][k] =  l2_gf_f[i][k];
			z[i][k] =  l2_gf_f[i][k];
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<30; i++) {
		for (k = 0; k<30; k++) {
			temp_resf[i] = g[i][k] + h[i][k] + j[i][k] + x[i][k] + y[i][k] + b[i][k] + c[i][k] + d[i][k] + e[i][k] + z[i][k];
		}
	}

}


void mem_elops_par_arr_ASSIGN_l3(int *a, int b[10][10][10], int c[10][10][10], int d[10][10][10], int e[10][10][10], int f[10][10][10], int g[10][10][10], int h[10][10][10], int j[10][10][10], int x[10][10][10], int y[10][10][10], int z[10][10][10]) {


	int i = 0, k = 0, n = 0, lf[10][10][10];

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] =  g[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] ;
				g[i][k][n] = f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] ;
				g[i][k][n] = f[i][k][n] ;
				j[i][k][n] = f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] ;
				g[i][k][n] = f[i][k][n] ;
				j[i][k][n] = f[i][k][n] ;
				x[i][k][n] = f[i][k][n] ;
				y[i][k][n] = f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	};



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = f[i][k][n] ;
				g[i][k][n] = f[i][k][n] ;
				j[i][k][n] = f[i][k][n] ;
				x[i][k][n] = f[i][k][n] ;
				y[i][k][n] = f[i][k][n] ;
				b[i][k][n] = f[i][k][n] ;
				c[i][k][n] = f[i][k][n] ;
				d[i][k][n] = f[i][k][n] ;
				e[i][k][n] = f[i][k][n] ;
				z[i][k][n] = f[i][k][n] ;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|const");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|const|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  25;
				g[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|const|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  25;
				g[i][k][n] =  25;
				j[i][k][n] =  25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|const|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =25;
				g[i][k][n] =25;
				j[i][k][n] =25;
				x[i][k][n] =25;
				y[i][k][n] =25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|const|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = 25;
				g[i][k][n] = 25;
				j[i][k][n] = 25;
				x[i][k][n] = 25;
				y[i][k][n] = 25;
				b[i][k][n] = 25;
				c[i][k][n] = 25;
				d[i][k][n] = 25;
				e[i][k][n] = 25;
				z[i][k][n] = 25;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = (*a);
				g[i][k][n] = (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  (*a);
				g[i][k][n] =  (*a);
				j[i][k][n] =  (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  (*a);
				g[i][k][n] =  (*a);
				j[i][k][n] =  (*a);
				x[i][k][n] =  (*a);
				y[i][k][n] =  (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = (*a);
				g[i][k][n] = (*a);
				j[i][k][n] = (*a);
				x[i][k][n] = (*a);
				y[i][k][n] = (*a);
				b[i][k][n] = (*a);
				c[i][k][n] = (*a);
				d[i][k][n] = (*a);
				e[i][k][n] = (*a);
				z[i][k][n] = (*a);
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  i;
				g[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = i;
				g[i][k][n] = i;
				j[i][k][n] = i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  i;
				g[i][k][n] =  i;
				j[i][k][n] =  i;
				x[i][k][n] =  i;
				y[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  i;
				g[i][k][n] =  i;
				j[i][k][n] =  i;
				x[i][k][n] =  i;
				y[i][k][n] =  i;
				b[i][k][n] =  i;
				c[i][k][n] =  i;
				d[i][k][n] =  i;
				e[i][k][n] =  i;
				z[i][k][n] =  i;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = lf[i][k][n];
				g[i][k][n] = lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  lf[i][k][n];
				g[i][k][n] =  lf[i][k][n];
				j[i][k][n] =  lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = lf[i][k][n];
				g[i][k][n] = lf[i][k][n];
				j[i][k][n] = lf[i][k][n];
				x[i][k][n] = lf[i][k][n];
				y[i][k][n] = lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|loc_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  lf[i][k][n];
				g[i][k][n] =  lf[i][k][n];
				j[i][k][n] =  lf[i][k][n];
				x[i][k][n] =  lf[i][k][n];
				y[i][k][n] =  lf[i][k][n];
				b[i][k][n] =  lf[i][k][n];
				c[i][k][n] =  lf[i][k][n];
				d[i][k][n] =  lf[i][k][n];
				e[i][k][n] =  lf[i][k][n];
				z[i][k][n] =  lf[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_var");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] =  g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_var|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  g_a;
				g[i][k][n] =  g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_var|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = g_a;
				g[i][k][n] = g_a;
				j[i][k][n] = g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_var|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  g_a;
				g[i][k][n] =  g_a;
				j[i][k][n] =  g_a;
				x[i][k][n] =  g_a;
				y[i][k][n] =  g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_var|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = g_a;
				g[i][k][n] = g_a;
				j[i][k][n] = g_a;
				x[i][k][n] = g_a;
				y[i][k][n] = g_a;
				b[i][k][n] = g_a;
				c[i][k][n] = g_a;
				d[i][k][n] = g_a;
				e[i][k][n] = g_a;
				z[i][k][n] = g_a;
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_arr");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				f[i][k][n] = l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = f[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_arr|seq_stat=2");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  l3_gf_f[i][k][n];
				g[i][k][n] =  l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n];
			}
		}
	}


	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_arr|seq_stat=3");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] =  l3_gf_f[i][k][n];
				g[i][k][n] =  l3_gf_f[i][k][n];
				j[i][k][n] =  l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_arr|seq_stat=5");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = l3_gf_f[i][k][n];
				g[i][k][n] = l3_gf_f[i][k][n];
				j[i][k][n] = l3_gf_f[i][k][n];
				x[i][k][n] = l3_gf_f[i][k][n];
				y[i][k][n] = l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n];
			}
		}
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|simple|lvl=3|glob_arr|seq_stat=10");

	t_start = get_timer_value();

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				h[i][k][n] = l3_gf_f[i][k][n];
				g[i][k][n] = l3_gf_f[i][k][n];
				j[i][k][n] = l3_gf_f[i][k][n];
				x[i][k][n] = l3_gf_f[i][k][n];
				y[i][k][n] = l3_gf_f[i][k][n];
				b[i][k][n] = l3_gf_f[i][k][n];
				c[i][k][n] = l3_gf_f[i][k][n];
				d[i][k][n] = l3_gf_f[i][k][n];
				e[i][k][n] = l3_gf_f[i][k][n];
				z[i][k][n] = l3_gf_f[i][k][n];
			}
		}
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)(i*k*n);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<10; i++) {
		for (k = 0; k<10; k++) {
			for (n = 0; n<10; n++) {
				temp_resf[i] = g[i][k][n] + h[i][k][n] + j[i][k][n] + x[i][k][n] + y[i][k][n] + b[i][k][n] + c[i][k][n] + d[i][k][n] + e[i][k][n] + z[i][k][n];
			}
		}
	}


}

void mem_elops_par_arr_ASSIGN_cmplx(int *f, int *g) {


	int i = 0;

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_par_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=0");

	t_start = get_timer_value();

	for (i = 0; i<1000; i++) {
		f[i] = g[i + 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|complex|lvl=1|add_nr=0|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}




	strcpy(op_name, "MEM_par_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10 + i] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|complex|lvl=1|add_nr=2|mul_nr=1");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10 + i + 10] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}



	strcpy(op_name, "MEM_par_arr - ASSIGN|complex|lvl=1|add_nr=1|mul_nr=2");

	t_start = get_timer_value();

	for (i = 0; i<100; i++) {
		f[i] = g[i * 10 + i * 5] ;
	}

	t_end = get_timer_value();
	duration = (t_end - t_start) / (double)i;
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

	for (i = 0; i<1000; i++) {
		temp_resf[i] = f[i];
	}
}

void mem_elops_par_arr_BLOCK(int *f, int *g) {

	

	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_par_arr - BLOCK");

	t_start = get_timer_value();


	memcpy(g, f, 1000 * sizeof(int));

	t_end = get_timer_value();
	duration = (t_end - t_start);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);
}


void mem_elops_par_var_PROC() {

	int i,a;
	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_par_var - PROC");

	t_start = get_timer_value();

	for (i = 0; i < 1000; i++) {
		a = rvpdummy_func(&i);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

}

void mem_elops_par_arr_PROC() {

	int i;
	int f[1000], g[1000];
	char op_name[100];

	long long t_start, t_end;
	double duration = 0.0;
	double exec_time_s = 0.0;

	int *mem_loc = 0; //define according to platform

	timer_setup();

	strcpy(op_name, "MEM_par_arr - PROC");

	t_start = get_timer_value();

	for (i = 0; i < 1000; i++) {
		f[i] = *rpdummy_func(&g[i]);
	}

	t_end = get_timer_value();
	duration = (t_end - t_start);
	exec_time_s = duration / TICKS_PER_SEC;

	store_exec_time(mem_loc, exec_time_s, op_name);

}


int rvpdummy_func(int *a) {
	return *a;
}
int *rpdummy_func(int *a) {
	return &a;
}