#include <stdlib.h>
#define TICKS_PER_SEC 333333344 // set value according to platform



extern double g_a, g_b, g_c, g_d, g_e, g_f, g_g, g_h, g_j, g_x, g_y;

extern double gf_b[1000], gf_c[1000], gf_d[1000], gf_e[1000], gf_f[2000], gf_g[1000], gf_h[1000], gf_j[1000], gf_x[1000], gf_y[1000], gf_z[1000];

extern double l2_gf_b[30][30], l2_gf_c[30][30], l2_gf_d[30][30], l2_gf_e[30][30], l2_gf_f[30][30], l2_gf_g[30][30], l2_gf_h[30][30], l2_gf_j[30][30], l2_gf_x[30][30], l2_gf_y[30][30], l2_gf_z[30][30];

extern double l3_gf_b[10][10][10], l3_gf_c[10][10][10], l3_gf_d[10][10][10], l3_gf_e[10][10][10], l3_gf_f[10][10][10], l3_gf_g[10][10][10], l3_gf_h[10][10][10], l3_gf_j[10][10][10], l3_gf_x[10][10][10], l3_gf_y[10][10][10], l3_gf_z[10][10][10];

extern double volatile temp_res, temp_resf[2000];

void timer_setup(); //implement according to platform

long long get_timer_value(); //implement according to platform

void store_exec_time(int *mem_loc, double exec_time_s, char *op_name);

void fp_elops_loc_var_ADD();
void fp_elops_loc_arr_ADD_l1();
void fp_elops_loc_arr_ADD_l2();
void fp_elops_loc_arr_ADD_l3();
void fp_elops_loc_arr_ADD_cmplx();
	 
void fp_elops_loc_var_MUL();
void fp_elops_loc_arr_MUL_l1();
void fp_elops_loc_arr_MUL_l2();
void fp_elops_loc_arr_MUL_l3();
void fp_elops_loc_arr_MUL_cmplx();

void fp_elops_loc_var_DIV();
void fp_elops_loc_arr_DIV_l1();
void fp_elops_loc_arr_DIV_l2();
void fp_elops_loc_arr_DIV_l3();
void fp_elops_loc_arr_DIV_cmplx();
	 

void fp_elops_glob_var_ADD();
void fp_elops_glob_arr_ADD_l1();
void fp_elops_glob_arr_ADD_l2();
void fp_elops_glob_arr_ADD_l3();
void fp_elops_glob_arr_ADD_cmplx();
	
void fp_elops_glob_var_MUL();
void fp_elops_glob_arr_MUL_l1();
void fp_elops_glob_arr_MUL_l2();
void fp_elops_glob_arr_MUL_l3();
void fp_elops_glob_arr_MUL_cmplx();

void fp_elops_glob_var_DIV();
void fp_elops_glob_arr_DIV_l1();
void fp_elops_glob_arr_DIV_l2();
void fp_elops_glob_arr_DIV_l3();
void fp_elops_glob_arr_DIV_cmplx();
	

void fp_elops_par_var_ADD(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y);
void fp_elops_par_arr_ADD_l1(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y, double *z);
void fp_elops_par_arr_ADD_l2(double *a, double b[30][30], double c[30][30], double d[30][30], double e[30][30], double f[30][30], double g[30][30], double h[30][30], double j[30][30], double x[30][30], double y[30][30], double z[30][30]);
void fp_elops_par_arr_ADD_l3(double *a, double b[10][10][10], double c[10][10][10], double d[10][10][10], double e[10][10][10], double f[10][10][10], double g[10][10][10], double h[10][10][10], double j[10][10][10], double x[10][10][10], double y[10][10][10], double z[10][10][10]);
void fp_elops_par_arr_ADD_cmplx(double *f, double *g);

void fp_elops_par_var_MUL(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y);
void fp_elops_par_arr_MUL_l1(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y, double *z);
void fp_elops_par_arr_MUL_l2(double *a, double b[30][30], double c[30][30], double d[30][30], double e[30][30], double f[30][30], double g[30][30], double h[30][30], double j[30][30], double x[30][30], double y[30][30], double z[30][30]);
void fp_elops_par_arr_MUL_l3(double *a, double b[10][10][10], double c[10][10][10], double d[10][10][10], double e[10][10][10], double f[10][10][10], double g[10][10][10], double h[10][10][10], double j[10][10][10], double x[10][10][10], double y[10][10][10], double z[10][10][10]);
void fp_elops_par_arr_MUL_cmplx(double *f, double *g);

void fp_elops_par_var_DIV(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y);
void fp_elops_par_arr_DIV_l1(double *a, double *b, double *c, double *d, double *e, double *f, double *g, double *h, double *j, double *x, double *y, double *z);
void fp_elops_par_arr_DIV_l2(double *a, double b[30][30], double c[30][30], double d[30][30], double e[30][30], double f[30][30], double g[30][30], double h[30][30], double j[30][30], double x[30][30], double y[30][30], double z[30][30]);
void fp_elops_par_arr_DIV_l3(double *a, double b[10][10][10], double c[10][10][10], double d[10][10][10], double e[10][10][10], double f[10][10][10], double g[10][10][10], double h[10][10][10], double j[10][10][10], double x[10][10][10], double y[10][10][10], double z[10][10][10]);
void fp_elops_par_arr_DIV_cmplx(double *f, double *g);

